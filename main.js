/* Please make sure the settings of resolution all set with same value
 * - config.xml
 * - lib/platform.js 	:: Platform.WIDTH & Platform.HEIGHT
 * - main.css			:: body & scene-container
 */

/* File: main.js
 *
 * Description:
 * The loader file to prepare while JSFramwork system
 *
 * Author:
 * I Made Krisna (made.krisna@samsung.com)
 *
 * Changelog:
 * - 2012-12-26 Initial Code (made.krisna)
 * - 2013-05-10 Add QUnit Unit Testing Feature (made.krisna)
 */

/* Class: Main
 * Main Class
 */

/* Constructor: Main
 * Initialize the Main Class
 */ 
var Main = new Object();

Main.EnableUnitTest = false;

/* Event: onLoad
 * The first called function after application launched, do preparation inside here
 */
Main.onLoad = function(){
	document.getElementById("anchor").focus();
	Platform.WidgetAPI.sendReadyEvent();
	
	/* Do preparation for apps in this timeout block */
	var runApps = null;
	runApps = setTimeout(function(){
		Platform.InitDebug('production');					// Mandatory, possible parameter: 'debugging', 'development', 'production'
		Platform.InitSEF();									// Mandatory						
				
		Analytics.initialize('ga-only','UA-40094004-7');	// UA-40094004-7 registered in analytics.google.com
		
		SceneManager.LoadPlugin('helpbar');
		SceneManager.LoadPlugin('loading');
		SceneManager.LoadPlugin('popup');
		Helpbar.Init();
		Loading.init();
		Popup.Init();
		
		Main.SetNetworkCheckTimer();
		
		if (Main.EnableUnitTest === true){
			document.getElementById('qunit').style.display = 'block';
		}
		
		SceneManager.Show('Splash');					// Look at scenes folder
		SceneManager.Focus('Splash');		
		
		/* Clean Application Runner Object */
		clearTimeout(runApps);
		runApps = null;
		
		Platform.Debug('[Main.onLoad -> runApps] Sequence Completed!', 'green');
	}, 1000);	
};

/* Event: onUnload
 * The function which called before application closed, do clean-up here
 */
Main.onUnload = function(){
	if (Platform.SEF.Player){
		Platform.SEF.Player.Stop();
	}
};

/* Event: keyDown
 * The function called by system if there is a user input
 */
Main.keyDown = function(){
	var KeyCode = event.keyCode;
	if (Popup.IsActive === true){
		Popup.KeyHandler(KeyCode);
	}
	else if (SceneManager.ActiveScene !== null){
		SceneManager.Collection[SceneManager.ActiveScene].handleKeyDown(KeyCode);
	}
	else{
		Platform.Debug('[Main.keyDown] Error: No Active Scene for Key Handler', 'red');
	}	
};

Main.SetNetworkCheckTimer = function(){
	setInterval(function(){						// network checking
		var status = Platform.CheckNetwork();
		if(!status && !Popup.IsActive){
			AppMgr.NetworkIsOn	= false;
			Popup.Show(function(){
				if(SceneManager.ActiveScene != 'Home'){
					SceneManager.Hide(SceneManager.ActiveScene);
				}
				SceneManager.Show('Home');
				SceneManager.Focus('Home');							
			});			
		}
		else if(status){
			AppMgr.NetworkIsOn	= true;
			if(!AppMgr.IsGettingGallery){
				Platform.Debug('Resuming Data Retrieve...', 'lime');
				AppMgr.GetNextGallery();
			}
		}
		//Platform.Debug('Checking network, status = ' + status, 'yellow');
	}, 3000);
};

Main.UnitTest = function(testTitle, testFunction, reset){
	if (reset === true){
		QUnit.reset();
		QUnit.init();
		QUnit.start();		
	}
	test(testTitle, testFunction);
};
