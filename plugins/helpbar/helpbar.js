var Helpbar		= new Object();

Helpbar.IMAGES_ENUM = {
	UP_KEY				: 'u.png',
	DOWN_KEY			: 'd.png',
	
	UP_DOWN				: 'ud.png',	
	LEFT_RIGHT			: 'lr.png',
	LEFT_RIGHT_UP_DOWN	: 'move.png',
	
	ENTER_KEY			: 'enter.png',
	RETURN_KEY			: 'return.png',
	
	FF_KEY				: 'ff.png',
	RW_KEY				: 'rw.png',
	FF_RW				: 'ffrw.png',	
	
	PAUSE_KEY			: 'pause.png',
	PLAY_KEY			: 'play.png',	
	
	STOP_KEY			: 'stop.png',	
	
	RED_KEY				: 'red.png',
	GREEN_KEY			: 'green.png',
	YELLOW_KEY			: 'yellow.png',
	BLUE_KEY			: 'blue.png'
};

Helpbar.Inited	= false;
Helpbar.Object	= new Array();

Helpbar.Init = function(){
	if(!document.getElementById('scenemanager_helpbar')){
		$('body').append('<div id="scenemanager_helpbar" class="helpbar-container"></div>');
	}
	Helpbar.Inited = true;
};

Helpbar.Update = function(id){
	//Helpbar.Debug('Object ID = ' + id);	
	if(Helpbar.Inited){
		var helpbar	= '';
		var index = 0;	
		
		for(var n = 0; n < Helpbar.Object.length; n++){
			if(Helpbar.Object[n].id == id){
				index = n;
				//Helpbar.Debug('Object Index = ' + index);
				break;
			}
		}
		
		if(Helpbar.Object && Helpbar.Object[index] && Helpbar.Object[index].data){
			//Helpbar.Debug('Help Count = ' + Helpbar.Object[index].data.length);
			for(var n=0; n < Helpbar.Object[index].data.length; n++){
				helpbar += '<div id="helpbar-'+n+'" class="item">';
				helpbar += 		'<img class="image" src="plugins/helpbar/images/'+Helpbar.Object[index].data[n].image+'"/>';
				helpbar += 		'<div class="text">'+Helpbar.Object[index].data[n].text+'</div>';
				helpbar += 	'</div>';
			}
		}
		Platform.WidgetAPI.putInnerHTML(document.getElementById('scenemanager_helpbar'), helpbar);
		Helpbar.AddListener(index);
		Helpbar.Show();
	}
	else{
		Helpbar.Debug('Helpbar is not inited yet!');
	}
};

Helpbar.AddObject = function(object){
	for(var n = 0; n < Helpbar.Object.length; n++){
		if(Helpbar.Object[n].id == object.id){
			Helpbar.Debug('This object has been existed!!!');
			return;
		}
	}
	Helpbar.Object.push(object);
};

Helpbar.AddListener = function(index){
	for(var n = 0; n < Helpbar.Object[index].data.length; n++){
		if(Helpbar.Object[index].data[n].onclick){
			document.getElementById('helpbar-'+n).addEventListener('click', Helpbar.Object[index].data[n].onclick, false);
			document.getElementById('helpbar-'+n).style.cursor = 'pointer';
		}
		else{
			document.getElementById('helpbar-'+n).style.cursor = 'default';
		}
	}
};

Helpbar.Show = function(){
	$('#scenemanager_helpbar').css('display','block');
};

Helpbar.Hide = function(){
	$('#scenemanager_helpbar').css('display','none');
};

Helpbar.Debug = function(message){
	Platform.Debug('[HELPBAR] >>> ' + message);
};