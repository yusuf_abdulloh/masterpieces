var Loyalty = new Object();

Loyalty.DEBUG 		= true;

Loyalty.ENDPOINT	= 'http://loyalty.seinsmarttv.com/api/';
Loyalty.API			= {
	CHECKAPPS 	: Loyalty.ENDPOINT + 'checkapps',
	ACTIVITY	: Loyalty.ENDPOINT + 'activity'
};

Loyalty.Ajax		= null;

Loyalty.Data = {
	Devices: {
		DUID 	: null,
		Model	: null,
		Year	: null,
		Online	: null
	},
	Apps:{
		ID		: null,
		Name	: null,
		Enabled	: false
	}
};

Loyalty.init = function(){
	Loyalty.Data.Devices.DUID 	= Platform.GetUID();
	Loyalty.Data.Devices.Model	= Platform.GetDeviceModel();
	Loyalty.Data.Devices.Year	= Platform.GetDeviceYear();	
	Loyalty.Data.Apps.ID		= Platform.WidgetID;
	
	Loyalty.Debug('Initialized');
	Loyalty.Debug('--- Device DUID: ' 	+ Loyalty.Data.Devices.DUID);
	Loyalty.Debug('--- Device Model: ' 	+ Loyalty.Data.Devices.Model);
	Loyalty.Debug('--- Device Year: '	+ Loyalty.Data.Devices.Year);
	Loyalty.Debug('--- Apps ID: '		+ Loyalty.Data.Apps.ID);
};

Loyalty.trigger = function(code){
	$.ajax({
		url			: Loyalty.API.ACTIVITY,
		type		: 'post',
		dataType	: 'json',
		timeout		: 30000,
		data		: {
			'loyalty_duid'		: Loyalty.Data.Devices.DUID,
			'loyalty_activity'	: code,
			'loyalty_widgetid'	: Loyalty.Data.Apps.ID			
		},		
		complete	: function (xhr, textstatus){						
			if (textstatus === 'timeout'){
				Loyalty.Debug('Activity Trigger Timeout');
			}
			else{
				Loyalty.Debug(xhr.status);							
			}		
		}
	});
};

Loyalty.abortAjax = function(){
	if (Loyalty.Ajax){
		Loyalty.Ajax.abort();
		Loyalty.Ajax = null;
	}
};

Loyalty.Debug = function(message){
	Platform.Debug('[LOYALTY SERVER DRIVER] ' + message);
};