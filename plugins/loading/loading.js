var Loading = new Object();

Loading.IsActive = false;

Loading.init = function(){
	$('body').append('<div id="scenemanager_loading" style="display:none" class="loading-window"><img src="plugins/loading/loading.gif" class="loading-image" /></div>');
};

Loading.show = function(){
	if (Loading.IsActive === false){
		$('#scenemanager_loading').css('display', 'block');
		Loading.IsActive = true;
	}
};

Loading.hide = function(){
	if (Loading.IsActive === true){
		$('#scenemanager_loading').css('display', 'none');
		Loading.IsActive = false;
	}
};

