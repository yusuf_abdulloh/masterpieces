var Popup = new Object();

Popup.Element		= null;

Popup.IsActive 		= false;
Popup.Callback		= null;

Popup.Init = function(){
	Popup.Element		= document.getElementById('popup-container');
};

Popup.Show = function(callback){
	Popup.Callback					= callback;	
	Popup.Element.style.display		= 'block';	
	Popup.IsActive					= true;
};

Popup.Hide = function(){
	Popup.IsActive					= false;
	Popup.Element.style.display		= 'none';
};

Popup.KeyHandler = function(keyCode){
	if (keyCode === Platform.TvKey.KEY_RETURN || keyCode === Platform.TvKey.KEY_ENTER){
		Platform.Block();		
		Popup.Hide();
		if(Popup.Callback){
			Popup.Callback();
		}		
	}
};

Popup.mouseClick = function(){
	Popup.KeyHandler(Platform.TvKey.KEY_ENTER);
};