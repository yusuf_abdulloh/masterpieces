/* File: Home.js
 *
 * Description:
 * Scenes javascript file for Home
 *
 * Author:
 * Yusuf Abdulloh (yusuf.abd@samsung.com)
 *
 * Changelog:
 * - 2012-12-26 Initial Code (made.krisna)
 * - 2013-05-10 Scene Activation
 */
 
function Home(){
}

Home.prototype.Ajax				= null;
Home.prototype.MenuIndex 		= 0;
Home.prototype.PrevIndex 		= 0;
Home.prototype.IsAnimComplete	= true;
Home.prototype.IsFirstLoad		= true;
Home.prototype.ThumbTimer		= null;

Home.prototype.Html = {
		Thumb:[
		    null,
		    null,
		    null,
		    null,
		    null
		],
		Menu :[
			null,	//menu[0] = autoplay
			null,
			null,
			null,
			null
		],
		Highlight	: null
	};

Home.prototype.initialize = function(){
	this.Debug('initialize()');
	var self = this;
	var homehelpbar = {
			id	:'home',
			data:[
	    	  	{
	    	  		text	:'MOVE',
	    	  		image	:Helpbar.IMAGES_ENUM.LEFT_RIGHT_UP_DOWN,	    	  		
	    	  		onclick	:null
	    	  	},
	    	  	{
	    	  		text	:'SELECT',
	    	  		image	:Helpbar.IMAGES_ENUM.ENTER_KEY,	    	  		
	    	  		onclick	:function(){
	    	  			if(self.IsAnimComplete){
	    	  				self.EnterKeyHandler();
	    	  			}
	    	  		}
	    	  	},
	    	  	{
	    	  		text	:'RETURN',
	    	  		image	:Helpbar.IMAGES_ENUM.RETURN_KEY,	    	  		
	    	  		onclick	:function(){
	    	  			if(self.IsAnimComplete){
	    	  				self.GoToSmartHub();
	    	  			}
	    	  		}
	    	  	}    	  	
	    	 ]
	};
	Helpbar.AddObject(homehelpbar);	
	/*----------------------------------------------------------------------------*/
	this.Html.Menu[0]	= document.getElementById('home-autoplay');
	this.Html.Menu[1]	= document.getElementById('home-random');
	this.Html.Menu[2]	= document.getElementById('home-masterpieces');
	this.Html.Menu[3]	= document.getElementById('home-trending');
	this.Html.Menu[4]	= document.getElementById('home-about');
	this.Html.Highlight = document.getElementById('home-highlight');
	/*----------------------------------------------------------------------------*/
	this.Html.Thumb[0]	= document.getElementById('home-thumbnail-2');
	this.Html.Thumb[1]	= document.getElementById('home-thumbnail-1');
	this.Html.Thumb[2]	= document.getElementById('home-thumbnail-3');
	this.Html.Thumb[3]	= document.getElementById('home-thumbnail-0');
	this.Html.Thumb[4]	= document.getElementById('home-thumbnail-4');
	/*----------------------------------------------------------------------------*/
	this.Html.Menu[0].addEventListener('mouseover', function(){if(self.IsAnimComplete){self.MenuIndex = 0; self.Highlight();}}, false);
	this.Html.Menu[1].addEventListener('mouseover', function(){if(self.IsAnimComplete){self.MenuIndex = 1; self.Highlight();}}, false);
	this.Html.Menu[2].addEventListener('mouseover', function(){if(self.IsAnimComplete){self.MenuIndex = 2; self.Highlight();}}, false);
	this.Html.Menu[3].addEventListener('mouseover', function(){if(self.IsAnimComplete){self.MenuIndex = 3; self.Highlight();}}, false);
	this.Html.Menu[4].addEventListener('mouseover', function(){if(self.IsAnimComplete){self.MenuIndex = 4; self.Highlight();}}, false);	
	
	this.Html.Highlight.addEventListener('click', function(){if(self.IsAnimComplete){self.EnterKeyHandler();}}, false);
	/*----------------------------------------------------------------------------*/
	$('#home-container').css('opacity','0.0');
	$('#home-autoplay').css({'left':'122px', 'top':'418px','width':'200px','height':'64px'});
	$('#home-random').css({'left':'343px', 'top':'418px','width':'200px','height':'64px'});
	$('#home-masterpieces').css({'left':'705px', 'top':'220px','width':'233px','height':'219px'});
	$('#home-trending').css({'left':'983px', 'top':'220px','width':'233px','height':'219px'});
	$('#home-about').css({'left':'1022px', 'top':'615px','width':'196px','height':'53px'});
	$('#home-highlight').css({'left':'120px', 'top':'417px','width':'202px','height':'62px'});
	/*----------------------------------------------------------------------------*/
};

Home.prototype.handleShow = function(){
	this.Debug('handleShow()');
	Analytics.track('Masterpieces/Home');
	Helpbar.Update('home');	
	
	var self = this;
	this.IsAnimComplete = false;
	$('#home-container').animate({opacity:1}, 500, function(){
		self.Highlight();
		self.IsAnimComplete = true;		
	});
};

Home.prototype.handleFocus = function(){
	this.Debug('handleFocus()');
	if(this.IsFirstLoad){
		this.IsFirstLoad = false;
		if(!this.CheckAgreementFile()){
			SceneManager.Show('Terms');
			SceneManager.Focus('Terms');	
		}
		this.FillThumbnails();
	}	
};

Home.prototype.handleHide = function(){
	this.Debug('handleHide()');
};

Home.prototype.handleKeyDown = function(keyCode){
	//this.Debug('handleKeyDown(' + keyCode + ')');
	if(this.IsAnimComplete){
		switch (keyCode){
		case Platform.TvKey.KEY_UP:
			this.Debug('Key Up');
			this.UpKeyHandler();
			break;
		case Platform.TvKey.KEY_DOWN:
			this.Debug('Key Down');
			this.DownKeyHandler();
			break;
		case Platform.TvKey.KEY_LEFT:
			this.Debug('Key Left');
			this.LeftKeyHandler();
			break;
		case Platform.TvKey.KEY_RIGHT:
			this.Debug('Key Right');
			this.RightKeyHandler();
			break;
		case Platform.MouseEvent.SCROLL_UP:
			this.Debug('Mouse Up');
			break;
		case Platform.MouseEvent.SCROLL_DOWN:
			this.Debug('Mouse Down');
			break;
		case Platform.MouseEvent.SCROLL_LEFT:
			this.Debug('Mouse Left');
			break;
		case Platform.MouseEvent.SCROLL_RIGHT:
			this.Debug('Mouse Right');
			break;			
		case Platform.TvKey.KEY_ENTER:
		case Platform.TvKey.KEY_PANEL_ENTER:
			this.EnterKeyHandler();
			break;		
		case Platform.TvKey.KEY_RETURN:
		case Platform.TvKey.KEY_PANEL_RETURN:
			Platform.Block();							// Same as widgetAPI.blockNavigation(event);
			this.GoToSmartHub();
			break;
		case Platform.TvKey.KEY_EXIT:
			Platform.Block();
			Platform.WidgetAPI.sendExitEvent();
			break;
		}
	}
	else{
		Platform.Block();
		switch (keyCode){
		case Platform.TvKey.KEY_EXIT:
			this.Debug('Key Exit');
			Platform.WidgetAPI.sendExitEvent();
			break;
		}
	}
};

Home.prototype.LeftKeyHandler = function(){
	if(this.MenuIndex > 0 && this.MenuIndex < 4){
		this.MenuIndex--;
		this.Highlight();
	}	
};

Home.prototype.RightKeyHandler = function(){
	if(this.MenuIndex < 3){
		this.MenuIndex++;
		this.Highlight();
	}
};

Home.prototype.UpKeyHandler = function(){
	if(this.MenuIndex == 4){
		this.MenuIndex = this.PrevIndex;
		this.Highlight();
	}
};

Home.prototype.DownKeyHandler = function(){
	if(this.MenuIndex != 4){
		this.PrevIndex = this.MenuIndex;
		this.MenuIndex = 4;
		this.Highlight();
	}
};

Home.prototype.EnterKeyHandler = function(){
	var self = this;
	self.Html.Menu[self.MenuIndex].style.backgroundImage = 'url(resource/images/home/menu-'+self.MenuIndex+'-click.png)';
	setTimeout(function(){
		self.Html.Menu[self.MenuIndex].style.backgroundImage = 'url(resource/images/home/menu-'+self.MenuIndex+'.png)';
		switch(self.MenuIndex){
		case 0:
			self.AutoPlay();
			break;
		case 1:
			self.RandomPlay();
			break;
		case 2:
			if(Masterpieces.length){
				Wall.ActiveData		= Masterpieces;
				Wall.TotalWall		= Wall.ActiveData.length;
				Wall.WallType		= 'masterpieces';
				Wall.ActiveIndex	= 0;
				self.GoToTitlePage();
			}
			else{
				self.Debug('Data is not ready yet...');
			}
			break;
		case 3:			
			if(Trending[0].Arts.length){
				Wall.ActiveData		= Trending;
				Wall.TotalWall		= Wall.ActiveData.length;
				Wall.WallType		= 'trending';
				Wall.ActiveIndex	= 0;
				self.GoToDrawerPage();
			}
			else{
				self.Debug('Data is not ready yet...');
			}
			break;
		case 4:
			self.ShowAbout();
			break;
		default:
			break;		
		}
	},200);
};

Home.prototype.GoToSmartHub = function(){
	$('#home-container').animate({opacity:0}, 500, function(){		
		Platform.WidgetAPI.sendReturnEvent();
	});	
};

Home.prototype.GoToTitlePage = function(){
	var self = this;
	self.IsAnimComplete = false;
	$('#home-container').animate({opacity:0},500, function(){
		self.IsAnimComplete		= true;
		Autoplay.IsRandom		= false;
		AppMgr.TitleIsShownOnly	= false;
		SceneManager.Show('Title');
		SceneManager.Focus('Title');
		SceneManager.Hide('Home');
	});	
};

Home.prototype.GoToDrawerPage = function(){
	var self = this;
	self.IsAnimComplete = false;
	$('#home-container').animate({opacity:0},500, function(){
		self.IsAnimComplete		= true;
		Autoplay.IsRandom		= false;
		AppMgr.TitleIsShownOnly	= false;
		AppMgr.BeforeDrawer		= 'Home';
		SceneManager.Show('Drawer');
		SceneManager.Focus('Drawer');			
		SceneManager.Hide('Home');
	});	
};

Home.prototype.ShowAbout = function(){
	SceneManager.Show('About');
	SceneManager.Focus('About');
};

Home.prototype.AutoPlay = function(){
	var self = this;
	if(Masterpieces.length){
		Autoplay.IsSet 			= true;
		Autoplay.IsRandom		= false;
		Wall.ActiveData			= Masterpieces;
		Wall.WallType			= 'masterpieces';
		Wall.ActiveIndex		= 0;
		Wall.SelectedImage		= 0;
		AppMgr.PrevScene		= 'Home';
		AppMgr.TitleIsShownOnly	= true;
		SceneManager.Show('Title');
		SceneManager.Focus('Title');
		SceneManager.Hide('Home');
		
		self.IsAnimComplete = false;		// for scene changing, prevent unexpected action
		setTimeout(function(){
			SceneManager.Collection['Title'].HideOnAutoplay(function(){		//add fade out effect
				SceneManager.Show('Bifold');
				SceneManager.Focus('Bifold');
				$('#bifold-container').animate({opacity:1},500, function(){		
					self.IsAnimComplete = true;
					SceneManager.Hide('Title');
				});
			});	
		},10000);
	}
};

Home.prototype.RandomPlay = function(){
	if(Masterpieces.length){
		Autoplay.IsSet 		= true;
		Autoplay.IsRandom	= true;
		Wall.ActiveData		= Masterpieces;
		Wall.WallType		= 'masterpieces';
		Wall.ActiveIndex	= 0;
		AppMgr.PrevScene	= 'Home';
		SceneManager.Show('Bifold');
		SceneManager.Focus('Bifold');
		SceneManager.Hide('Home');
	}
};

Home.prototype.Highlight = function(){
	var newLeft		= null;
	var newTop		= null;
	var newWidth	= null;
	var newHeight	= null;
	var self		= this;
	
	/*------------- calculate highlight position -------------------*/
	newLeft		= parseInt(this.Html.Menu[this.MenuIndex].style.left);
	newTop		= parseInt(this.Html.Menu[this.MenuIndex].style.top);
	newWidth	= parseInt(this.Html.Menu[this.MenuIndex].style.width);
	newHeight	= parseInt(this.Html.Menu[this.MenuIndex].style.height);
	
	if(this.MenuIndex == 0 || this.MenuIndex == 1){
		$('#home-highlight').css('border-radius', '6px');
		newLeft		+= 2;
		newTop		+= 2;
		newWidth	-= 10;
		newHeight	-= 10;
	}
	else{
		$('#home-highlight').css('border-radius', '0px');
		newWidth	-= 6;
		newHeight	-= 6;
	}
	
	newLeft		+= 'px';
	newTop		+= 'px';
	newWidth	+= 'px';
	newHeight	+= 'px';
	
	self.IsAnimComplete = false;
	$('#home-highlight').animate({left:newLeft, top:newTop, width:newWidth, height:newHeight}, 500, function(){
		self.IsAnimComplete = true;
	});
	/*--------------------------------------------------------------*/
};

Home.prototype.FillThumbnails = function(){
	var self = this;
	if(Trending[0].Arts.length){
		for(var n=0; n<5; n++){
			if(Trending[0].Arts[n] && Trending[0].Arts[n].Image){
				self.Html.Thumb[n].style.backgroundImage = 'url(' + Trending[0].Arts[n].Image + ')';
			}
			else{
				self.Html.Thumb[n].style.backgroundImage = 'url(resource/images/home/home-thumb.png)';
			}
		}
		if(self.ThumbTimer){
			clearInterval(self.ThumbTimer);
			self.ThumbTimer = null;
		}
	}
	else if(!self.ThumbTimer){
		self.ThumbTimer = setInterval(function(){
			self.Debug('Waiting thumbs...');
			self.FillThumbnails();
		},3000);
	}
};
/* ------------------------------ agreement function --------------------------------- */
Home.prototype.CheckAgreementFile = function(){
	var isUserAgreed	= false;
	var fsObj			= new FileSystem();
	var fObj			= null;
	var dValPath 		= fsObj.isValidCommonPath(curWidget.id);
	if (!dValPath) {
		fsObj.createCommonDir(curWidget.id);
		this.Debug('Common dir doesn\'t exist, creating...');
	}
	var fValPath	= fsObj.isValidCommonPath(curWidget.id + '/agreement.txt');
	if (!fValPath) {
		this.Debug('Common file doesn\'t exist, creating...');
		fObj	= fsObj.openCommonFile(curWidget.id + '/agreement.txt', 'w');
		fObj.writeAll('');
		fsObj.closeCommonFile(fObj);
	}
	fObj	= fsObj.openCommonFile(curWidget.id + '/agreement.txt', 'r');
	var agreement	= fObj.readAll();
	if(agreement){
		var data = eval('(' + agreement + ')');
		isUserAgreed = data.IsUserAgreed;		//load agreement
		if(isUserAgreed){
			return true;
		}
		else{
			return false;
		}
	}
	else{
		return false;
	}
};
/* ------------------------------ end of agreement function --------------------------------- */

Home.prototype.Debug = function(message){
	Platform.Debug('[SCENE >> Home] ' + message);
};