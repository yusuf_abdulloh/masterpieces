/* File: About.js
 *
 * Description:
 * Scenes javascript file for About
 *
 * Author:
 * Yusuf Abdulloh (yusuf.abd@samsung.com)
 *
 * Changelog:
 * - 2012-12-26 Initial Code (made.krisna)
 * - 2013-05-10 Scene Activation
 */
 
function About(){
}

About.prototype.IsAnimComplete	= true;

About.prototype.initialize = function(){
	this.Debug('initialize()');
	var self = this;
	$('#about-container').css('opacity', '0');
	$('#about-content-container').css('opacity', '0');
	
	document.getElementById('about-closebtn').addEventListener('click', function(){
		self.Hide();
	}, false);
};

About.prototype.handleShow = function(){
	this.Debug('handleShow()');
	Analytics.track('Masterpieces/About');
	Helpbar.Hide();
	this.Show();
};

About.prototype.handleFocus = function(){
	this.Debug('handleFocus()');	
};

About.prototype.handleHide = function(){
	this.Debug('handleHide()');
};

About.prototype.handleKeyDown = function(keyCode){
	this.Debug('handleKeyDown(' + keyCode + ')');
	if(this.IsAnimComplete){
		switch (keyCode){
			case Platform.TvKey.KEY_UP:
				this.Debug('Key Up');
				break;
			case Platform.TvKey.KEY_DOWN:
				this.Debug('Key Down');
				break;
			case Platform.TvKey.KEY_LEFT:
				this.Debug('Key Left');
				break;
			case Platform.TvKey.KEY_RIGHT:
				this.Debug('Key Right');
				break;
			case Platform.MouseEvent.SCROLL_UP:
				this.Debug('Mouse Up');
				break;
			case Platform.MouseEvent.SCROLL_DOWN:
				this.Debug('Mouse Down');
				break;
			case Platform.MouseEvent.SCROLL_LEFT:
				this.Debug('Mouse Left');
				break;
			case Platform.MouseEvent.SCROLL_RIGHT:
				this.Debug('Mouse Right');
				break;			
			case Platform.TvKey.KEY_ENTER:
			case Platform.TvKey.KEY_PANEL_ENTER:
			case Platform.TvKey.KEY_RETURN:
			case Platform.TvKey.KEY_PANEL_RETURN:
				Platform.Block();							// Same as widgetAPI.blockNavigation(event);
				this.Hide();
				break;
			case Platform.TvKey.KEY_EXIT:
				Platform.Block();
				Platform.WidgetAPI.sendExitEvent();
				break;
		}
	}
	else{
		Platform.Block();
		switch (keyCode){
		case Platform.TvKey.KEY_EXIT:
			this.Debug('Key Exit');
			Platform.WidgetAPI.sendExitEvent();
			break;
		}
	}
};

About.prototype.Show = function(){
	var self = this;
	self.IsAnimComplete = false;
	$('#about-container').animate({opacity:1}, 500, function(){
		$('#about-content-container').animate({opacity:1}, 500, function(){
			self.IsAnimComplete = true;
		});		
	});
};

About.prototype.Hide = function(){
	var self = this;
	$('#about-closebtn').css('background-image', 'url(resource/images/about/close-click.png)');
	setTimeout(function(){
		$('#about-closebtn').css('background-image', 'url(resource/images/about/close-sel.png)');
		self.IsAnimComplete = false;
		$('#about-content-container').animate({opacity:0}, 500, function(){
			$('#about-container').animate({opacity:0}, 500, function(){
				self.IsAnimComplete = true;
				Helpbar.Update('home');
				SceneManager.Focus('Home');
				SceneManager.Hide('About');
			});
		});
	}, 200);	
};

About.prototype.Debug = function(message){
	Platform.Debug('[SCENE >> About] ' + message);
};