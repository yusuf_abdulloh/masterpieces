/* File: Terms.js
 *
 * Description:
 * Scenes javascript file for Terms
 *
 * Author:
 * Yusuf Abdulloh (yusuf.abd@samsung.com)
 *
 * Changelog:
 * - 2012-12-26 Initial Code (made.krisna)
 * - 2013-05-10 Scene Activation
 */
 
function Terms(){
}

Terms.prototype.IsAnimComplete	= true;
Terms.prototype.ActiveButton	= 0;

Terms.prototype.initialize = function(){
	this.Debug('initialize()');
	var self = this;
	$('#terms-container').css('opacity', '0');
	$('#terms-content-container').css('opacity', '0');
	
	document.getElementById('terms-disagree').addEventListener('mouseover', function(){
		self.ActiveButton = 1;
		self.Highlight();
	}, false);
	document.getElementById('terms-disagree').addEventListener('click', function(){
		self.EnterKeyHandler();
	}, false);
	
	document.getElementById('terms-agree').addEventListener('mouseover', function(){
		self.ActiveButton = 0;
		self.Highlight();
	}, false);
	document.getElementById('terms-agree').addEventListener('click', function(){
		self.EnterKeyHandler();
	}, false);
};

Terms.prototype.handleShow = function(){
	this.Debug('handleShow()');
	Analytics.track('Masterpieces/UserAgreement');
	Helpbar.Hide();
	this.Show();
};

Terms.prototype.handleFocus = function(){
	this.Debug('handleFocus()');	
};

Terms.prototype.handleHide = function(){
	this.Debug('handleHide()');
};

Terms.prototype.handleKeyDown = function(keyCode){
	this.Debug('handleKeyDown(' + keyCode + ')');
	if(this.IsAnimComplete){
		switch (keyCode){
			case Platform.TvKey.KEY_UP:
				this.Debug('Key Up');
				break;
			case Platform.TvKey.KEY_DOWN:
				this.Debug('Key Down');
				break;
			case Platform.TvKey.KEY_LEFT:
				this.Debug('Key Left');
			case Platform.TvKey.KEY_RIGHT:
				this.Debug('Key Right');
				this.LeftRightKeyHandler();
				break;
			case Platform.MouseEvent.SCROLL_UP:
				this.Debug('Mouse Up');
				break;
			case Platform.MouseEvent.SCROLL_DOWN:
				this.Debug('Mouse Down');
				break;
			case Platform.MouseEvent.SCROLL_LEFT:
				this.Debug('Mouse Left');
				break;
			case Platform.MouseEvent.SCROLL_RIGHT:
				this.Debug('Mouse Right');				
				break;			
			case Platform.TvKey.KEY_ENTER:
			case Platform.TvKey.KEY_PANEL_ENTER:
				this.EnterKeyHandler();
				break;
			case Platform.TvKey.KEY_RETURN:
			case Platform.TvKey.KEY_PANEL_RETURN:
				Platform.Block();							// Same as widgetAPI.blockNavigation(event);
				this.ActiveButton	= 1;
				this.EnterKeyHandler();
				break;
			case Platform.TvKey.KEY_EXIT:
				Platform.Block();
				Platform.WidgetAPI.sendExitEvent();
				break;
		}
	}
	else{
		Platform.Block();
		switch (keyCode){
		case Platform.TvKey.KEY_EXIT:
			this.Debug('Key Exit');
			Platform.WidgetAPI.sendExitEvent();
			break;
		}
	}
};

Terms.prototype.LeftRightKeyHandler = function(){
	if(this.ActiveButton == 0){
		this.ActiveButton = 1;
	}
	else {
		this.ActiveButton = 0;
	}
	this.Highlight();
};

Terms.prototype.EnterKeyHandler = function(){
	var self = this;	
	switch(this.ActiveButton){
	case 0:
		$('#terms-agree').css('background-image', 'url(resource/images/terms/agree-click.png)');
		break;
	case 1:
		$('#terms-disagree').css('background-image', 'url(resource/images/terms/disagree-click.png)');
		break;	
	}
	
	setTimeout(function(){
		switch(self.ActiveButton){
		case 0:
			$('#terms-agree').css('background-image', 'url(resource/images/terms/agree-sel.png)');			
			self.SaveAgreement();	//command to write common data
			self.Hide();
			break;
		case 1:
			$('#terms-disagree').css('background-image', 'url(resource/images/terms/disagree-sel.png)');
			//not writing common data
			Platform.WidgetAPI.sendReturnEvent();
			break;
		}		
	}, 200);	
};

Terms.prototype.Show = function(){
	var self = this;
	self.Highlight();
	self.IsAnimComplete = false;
	$('#terms-container').animate({opacity:1}, 500, function(){
		$('#terms-content-container').animate({opacity:1}, 500, function(){
			self.IsAnimComplete = true;
		});		
	});
};

Terms.prototype.Hide = function(){
	var self = this;
	self.IsAnimComplete = false;
	$('#terms-content-container').animate({opacity:0}, 500, function(){
		$('#terms-container').animate({opacity:0}, 500, function(){
			self.IsAnimComplete = true;
			Helpbar.Update('home');
			SceneManager.Focus('Home');
			SceneManager.Hide('Terms');
		});
	});	
};

Terms.prototype.Highlight = function(){
	$('#terms-disagree').css('background-image', 'url(resource/images/terms/disagree.png)');
	$('#terms-agree').css('background-image', 'url(resource/images/terms/agree.png)');
	switch(this.ActiveButton){
	case 0:
		$('#terms-agree').css('background-image', 'url(resource/images/terms/agree-sel.png)');
		break;
	case 1:
		$('#terms-disagree').css('background-image', 'url(resource/images/terms/disagree-sel.png)');
		break;
	}
};

Terms.prototype.SaveAgreement = function(){
	var fsObj	= new FileSystem();
	var fObj	= fsObj.openCommonFile(curWidget.id + '/agreement.txt', 'w');
	var data	= new Object();	
	data.IsUserAgreed = true;
	
	fObj.writeAll($.toJSON(data));
	fsObj.closeCommonFile(fObj);
};

Terms.prototype.Debug = function(message){
	Platform.Debug('[SCENE >> Terms] ' + message);
};