/* File: Splash.js
 *
 * Description:
 * Scenes javascript file for Splash
 *
 * Author:
 * Yusuf Abdulloh (yusuf.abd@samsung.com)
 *
 * Changelog:
 * - 2012-12-26 Initial Code (made.krisna)
 * - 2013-05-10 Scene Activation
 */
 
function Splash(){
}

Splash.prototype.initialize = function(){
	this.Debug('initialize()');
	$('#splash-logo').css('opacity','0.0');
};

Splash.prototype.handleShow = function(){
	this.Debug('handleShow()');
	Analytics.track('Masterpieces/SplashScreen');
	Helpbar.Hide();
	
	var self = this;
	$('#splash-logo').animate({opacity:1}, 2000, function(){
		$('#splash-samsungart').animate({opacity:1}, 2000, function(){
			$('#splash-container').animate({opacity:0}, 500, function(){
				SceneManager.Show('Home');
				SceneManager.Focus('Home');
				SceneManager.Hide('Splash');
			});
		});			
	});
	
	AppMgr.GetNextGallery();
};

Splash.prototype.handleFocus = function(){
	this.Debug('handleFocus()');	
};

Splash.prototype.handleHide = function(){
	this.Debug('handleHide()');
};

Splash.prototype.handleKeyDown = function(keyCode){
	this.Debug('handleKeyDown(' + keyCode + ')');
	switch (keyCode){
		case Platform.TvKey.KEY_UP:
			this.Debug('Key Up');
			break;
		case Platform.TvKey.KEY_DOWN:
			this.Debug('Key Down');
			break;
		case Platform.TvKey.KEY_LEFT:
			this.Debug('Key Left');
			break;
		case Platform.TvKey.KEY_RIGHT:
			this.Debug('Key Right');
			break;
		case Platform.MouseEvent.SCROLL_UP:
			this.Debug('Mouse Up');
			break;
		case Platform.MouseEvent.SCROLL_DOWN:
			this.Debug('Mouse Down');
			break;
		case Platform.MouseEvent.SCROLL_LEFT:
			this.Debug('Mouse Left');
			break;
		case Platform.MouseEvent.SCROLL_RIGHT:
			this.Debug('Mouse Right');
			break;			
		case Platform.TvKey.KEY_ENTER:
		case Platform.TvKey.KEY_PANEL_ENTER:
			break;		
		case Platform.TvKey.KEY_RETURN:
		case Platform.TvKey.KEY_PANEL_RETURN:
			Platform.Block();							// Same as widgetAPI.blockNavigation(event);
			Platform.WidgetAPI.sendReturnEvent();
			break;
		case Platform.TvKey.KEY_EXIT:
			Platform.Block();
			Platform.WidgetAPI.sendExitEvent();
			break;
	}
};

Splash.prototype.Debug = function(message){
	Platform.Debug('[SCENE >> Splash] ' + message);
};