/* File: Drawer.js
 *
 * Description:
 * Scenes javascript file for Intro
 *
 * Author:
 * Yusuf Abdulloh (yusuf.abd@samsung.com)
 *
 * Changelog:
 * - 2012-12-26 Initial Code (made.krisna)
 * - 2013-05-10 Scene Activation (yusuf.abd)
 */
 
var DrawerSelf	= null;

function Drawer(){
}

Drawer.prototype.IsAnimComplete	= true;
Drawer.prototype.ActivePart		= 1;
Drawer.prototype.ImageIndex 	= 0;
Drawer.prototype.ImageWidth		= 1080;
Drawer.prototype.TitleTimeout	= null;

Drawer.prototype.Html = {
	Logo		: null,
	RightArrow	: null,
	LeftArrow	: null
};

Drawer.prototype.initialize = function(){
	this.Debug('initialize()');
	DrawerSelf	= this;
	var self = this;
	var drawerhelpbar = {
			id	:'drawer',
			data:[
	    	  	{
	    	  		text	:'SLIDE',
	    	  		image	:Helpbar.IMAGES_ENUM.LEFT_RIGHT,	    	  		
	    	  		onclick	:null
	    	  	},
	    	  	{
	    	  		text	:'SELECT',
	    	  		image	:Helpbar.IMAGES_ENUM.ENTER_KEY,	    	  		
	    	  		onclick	:function(){self.EnterKeyHandler();}
	    	  	},
	    	  	{
	    	  		text	:'RETURN',
	    	  		image	:Helpbar.IMAGES_ENUM.RETURN_KEY,	    	  		
	    	  		onclick	:function(){self.ReturnKeyHandler();}
	    	  	}    	  	
	    	 ]
	};
	Helpbar.AddObject(drawerhelpbar);
	
	this.Html.Logo			= document.getElementById('drawer-logo');
	this.Html.RightArrow	= document.getElementById('drawer-content-right-arrow');
	this.Html.LeftArrow		= document.getElementById('drawer-content-left-arrow');
	
	this.Html.Logo.addEventListener('mouseover', function(){
		self.ActivePart = 0;
		self.Highlight();
	}, false);
	this.Html.Logo.addEventListener('click', function(){self.EnterKeyHandler();}, false);
	
	this.Html.RightArrow.addEventListener('mouseover', function(){
		self.ActivePart = 1;
		self.Html.Logo.style.backgroundImage		= 'url(resource/images/drawer/M.png)';
		self.Html.RightArrow.style.backgroundImage	= 'url(resource/images/drawer/gallery-right-sel.png)';
	}, false);
	this.Html.RightArrow.addEventListener('mouseout', function(){
		self.Html.RightArrow.style.backgroundImage	= 'url(resource/images/drawer/gallery-right.png)';
	}, false);
	this.Html.RightArrow.addEventListener('click', function(){self.RightKeyHandler();}, false);
	
	this.Html.LeftArrow.addEventListener('mouseover', function(){
		self.ActivePart = 1;
		self.Html.Logo.style.backgroundImage		= 'url(resource/images/drawer/M.png)';
		self.Html.LeftArrow.style.backgroundImage	= 'url(resource/images/drawer/gallery-left-sel.png)';
	}, false);
	this.Html.LeftArrow.addEventListener('mouseout', function(){
		self.Html.LeftArrow.style.backgroundImage	= 'url(resource/images/drawer/gallery-left.png)';
	}, false);
	this.Html.LeftArrow.addEventListener('click', function(){self.LeftKeyHandler();}, false);
	
	$('#drawer-container').css('opacity', '0');
};

Drawer.prototype.handleShow = function(){
	this.Debug('handleShow()');
	Analytics.track('Masterpieces/Gallery');
	Helpbar.Update('drawer');
	var self = this;
	self.ActivePart	= 1;
	self.ImageIndex = Wall.SelectedImage;
	self.UpdateImages();
	self.IsAnimComplete = false;
	$('#drawer-container').animate({opacity:1}, 500, function(){
		self.IsAnimComplete = true;
	});
};

Drawer.prototype.handleFocus = function(){
	this.Debug('handleFocus()');
};

Drawer.prototype.handleHide = function(){
	this.Debug('handleHide()');
};

Drawer.prototype.handleKeyDown = function(keyCode){
	//this.Debug('handleKeyDown(' + keyCode + ')');
	if(this.IsAnimComplete){
		switch (keyCode){
			case Platform.TvKey.KEY_UP:
				this.Debug('Key Up');
				this.UpKeyHandler();
				break;
			case Platform.TvKey.KEY_DOWN:
				this.Debug('Key Down');
				this.DownKeyHandler();
				break;
			case Platform.TvKey.KEY_LEFT:
				this.Debug('Key Left');
				this.LeftKeyHandler();
				break;
			case Platform.TvKey.KEY_RIGHT:
				this.Debug('Key Right');
				this.RightKeyHandler();
				break;
			case Platform.TvKey.KEY_ENTER:
			case Platform.TvKey.KEY_PANEL_ENTER:
				this.EnterKeyHandler();
				break;		
			case Platform.TvKey.KEY_RETURN:
			case Platform.TvKey.KEY_PANEL_RETURN:
				Platform.Block();							// Same as widgetAPI.blockNavigation(event);
				this.ReturnKeyHandler();
				break;
			case Platform.TvKey.KEY_EXIT:
				Platform.Block();
				Platform.WidgetAPI.sendExitEvent();
				break;
		}
	}
	else{
		Platform.Block();
		switch (keyCode){
		case Platform.TvKey.KEY_EXIT:
			this.Debug('Key Exit');
			Platform.WidgetAPI.sendExitEvent();
			break;
		}
	}
};

Drawer.prototype.UpKeyHandler = function(){
	if(this.ActivePart == 1){
		this.ActivePart = 0;
		this.Highlight();
	}
};

Drawer.prototype.DownKeyHandler = function(){
	if(this.ActivePart == 0){
		this.ActivePart = 1;
		this.Highlight();
		this.Html.LeftArrow.style.backgroundImage = 'url(resource/images/drawer/gallery-left-sel.png)';
		this.Html.RightArrow.style.backgroundImage = 'url(resource/images/drawer/gallery-right.png)';
	}
};

Drawer.prototype.LeftKeyHandler = function(){
	if(this.ActivePart == 1){
		var self = this;
		if(this.ImageIndex > 0){
			this.ImageIndex--;
			this.Highlight();
			this.Html.LeftArrow.style.backgroundImage = 'url(resource/images/drawer/gallery-left-click.png)';
			this.Html.RightArrow.style.backgroundImage = 'url(resource/images/drawer/gallery-right.png)';
			setTimeout(function(){
				self.Html.LeftArrow.style.backgroundImage = 'url(resource/images/drawer/gallery-left-sel.png)';
			},200);
		}
		else {
			this.ReturnKeyHandler();
		}
	}
};

Drawer.prototype.RightKeyHandler = function(){
	if(this.ActivePart == 1){
		var self = this;
		if(this.ImageIndex < Wall.TotalImage - 1){
			this.ImageIndex++;
			this.Highlight();
			this.Html.LeftArrow.style.backgroundImage = 'url(resource/images/drawer/gallery-left.png)';
			this.Html.RightArrow.style.backgroundImage = 'url(resource/images/drawer/gallery-right-click.png)';
			setTimeout(function(){
				self.Html.RightArrow.style.backgroundImage = 'url(resource/images/drawer/gallery-right-sel.png)';
			},200);
		}
		else if(Wall.ActiveIndex < Wall.TotalWall - 1){
			do{
				Wall.ActiveIndex++;
			}
			while(Wall.ActiveData[Wall.ActiveIndex].Arts.length == 0);
			self.ImageIndex = 0;
			self.HandleNextContent();			
		}
		else{
			Wall.ActiveIndex = 0;
			self.ImageIndex = 0;
			self.HandleNextContent();
		}
	}	
};

Drawer.prototype.EnterKeyHandler = function(){
	var self = this;
	if(this.ActivePart == 0){
		self.Html.Logo.style.backgroundImage = 'url(resource/images/drawer/M-click.png)';
		setTimeout(function(){
			self.Html.Logo.style.backgroundImage = 'url(resource/images/drawer/M.png)';
			Wall.SelectedImage	= 0;
			self.IsAnimComplete = false;
			$('#drawer-container').animate({opacity:0}, 500, function(){
				self.IsAnimComplete = true;
				$('#drawer-content-container').css('left','0px');
				SceneManager.Show('Home');
				SceneManager.Focus('Home');
				SceneManager.Hide('Drawer');
			});
		}, 200);
	}
	else{
		Wall.SelectedImage	= this.ImageIndex;
		AppMgr.PrevScene	= 'Drawer';
		SceneManager.Show('Bifold');
		SceneManager.Focus('Bifold');
		SceneManager.Hide('Drawer');
	}
};

Drawer.prototype.ReturnKeyHandler = function(){
	var self = this;
	Wall.SelectedImage	= 0;
	self.IsAnimComplete = false;
	$('#drawer-container').animate({opacity:0}, 500, function(){
		self.IsAnimComplete = true;
		$('#drawer-content-container').css('left','0px');
		if(AppMgr.BeforeDrawer == 'Title'){
			AppMgr.TitleIsShownOnly	= false;
		}
		SceneManager.Show(AppMgr.BeforeDrawer);
		SceneManager.Focus(AppMgr.BeforeDrawer);
		SceneManager.Hide('Drawer');
	});
};

Drawer.prototype.Highlight = function(){
	var self		= this;
	/* reset display */
	self.Html.Logo.style.backgroundImage		= 'url(resource/images/drawer/M.png)';
	self.Html.RightArrow.style.backgroundImage	= 'url(resource/images/drawer/gallery-right.png)';
	self.Html.LeftArrow.style.backgroundImage	= 'url(resource/images/drawer/gallery-left.png)';
	
	if(self.ActivePart == 0){
		self.Html.Logo.style.backgroundImage = 'url(resource/images/drawer/M-sel.png)';
	}
	else{
		var leftPos		= null;
		leftPos		= -((this.ImageIndex+1)*this.ImageWidth) + (640+(this.ImageWidth/2)) + 'px';
		self.IsAnimComplete = false;
		$('#drawer-content-container').animate({left:leftPos}, 1000, function(){							
			self.IsAnimComplete = true;
		});
	}
};

Drawer.prototype.UpdateImages = function(){	
	var html		= '';
	var divLength	= 0;
	
	Wall.TotalImage = Wall.ActiveData[Wall.ActiveIndex].Arts.length;
	
	for(var m = 0; m < Wall.TotalImage; m++){
		html += '<div class="drawer-content-image-container">';				
		html += '<div id="drawer-content-image-light-'+m+'" class="drawer-content-image-light" ></div>';
		html += '<div id="drawer-content-image-shadow-'+m+'" class="drawer-content-image-shadow" ></div>';
		html += '<img id="drawer-content-image-'+m+'" class="drawer-content-image" onload="Drawer.OnImageLoaded(\''+m+'\')" src="'+Wall.ActiveData[Wall.ActiveIndex].Arts[m].Image+'" />';				
		html += '	<div id="drawer-content-image-title-container-'+m+'" class="drawer-content-image-title-container" >'+Wall.ActiveData[Wall.ActiveIndex].Arts[m].Title;
		html += '		<div class="drawer-content-image-title-artist">'+Wall.ActiveData[Wall.ActiveIndex].Arts[m].Artist.Name+'</div>';
		html += 	'</div>';
		html += '</div>';		
	}
	
	divLength = this.ImageWidth * Wall.TotalImage;
	$('#drawer-content-gallery').css('width', 705 + divLength + 'px');
	$('#drawer-content-container').css('width', 705 + divLength + 'px');	
	
	Platform.WidgetAPI.putInnerHTML(document.getElementById('drawer-content-gallery'), html);
	this.Highlight();
};

Drawer.prototype.OnImageClicked = function(id){	
	if(id == this.ImageIndex){
		this.EnterKeyHandler();
	}
};

Drawer.prototype.HandleNextContent = function(){
	var self = this;
	if(Wall.ActiveData[Wall.ActiveIndex].Type != 'trending'){
		self.IsAnimComplete = false;
		$('#drawer-container').animate({opacity:0},500, function(){		
			self.IsAnimComplete		= true;
			AppMgr.TitleIsShownOnly	= false;
			SceneManager.Show('Title');
			SceneManager.Focus('Title');
			SceneManager.Hide('Drawer');
		});
	}
	else{
		self.IsAnimComplete = false;
		$('#drawer-container').animate({opacity:0},500, function(){		
			self.ActivePart	= 1;
			self.UpdateImages();
			self.Highlight();
			$('#drawer-container').animate({opacity:1},500, function(){		
				self.IsAnimComplete = true;
			});
		});
	}
};

Drawer.prototype.Debug = function(message){
	Platform.Debug('[SCENE >> Drawer] ' + message);
};

/*-----------------------not prototype--------------------------*/

Drawer.OnImageLoaded = function(id){	
	var image			= document.getElementById('drawer-content-image-' + id);
	var imageLight		= document.getElementById('drawer-content-image-light-' + id);
	var imageShadow		= document.getElementById('drawer-content-image-shadow-' + id);
	var imagetitle		= document.getElementById('drawer-content-image-title-container-' + id);
	var imageLeft		= Math.round((1080 - image.width)/2);
	var imageTop		= Math.round((550 - image.height)/2);
	
	image.style.left			= imageLeft - 12 + 'px';
	image.style.top				= imageTop - 12 + 'px';
	
	imageLight.style.display	= 'block';
	
	imageShadow.style.left		= imageLeft - Math.ceil(0.065*image.width) + 'px';
	imageShadow.style.top		= imageTop + image.height + 10 + 'px';
	imageShadow.style.width		= Math.ceil(1.13*image.width) + 'px';
	imageShadow.style.display	= 'block';
	
	imagetitle.style.left		= imageLeft - 12 + 'px';
	imagetitle.style.maxWidth	= image.width + 24 - 20 + 'px';	//24 = 2xborder, 20 = 2xpadding
	
	image.addEventListener('click', function(){DrawerSelf.OnImageClicked(id);}, false);
};