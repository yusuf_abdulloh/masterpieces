/* File: Title.js
 *
 * Description:
 * Scenes javascript file for Title
 *
 * Author:
 * Yusuf Abdulloh (yusuf.abd@samsung.com)
 *
 * Changelog:
 * - 2012-12-26 Initial Code (made.krisna)
 * - 2013-05-10 Scene Activation
 */
 
function Title(){
}

Title.prototype.IsAnimComplete	= true;
Title.prototype.ActiveMenu		= 1;
Title.prototype.Html = {
	Logo	:null,
	Title	:null,
	Desc	:null,
	LArrow	:null,
	RArrow	:null,
	Next	:null,
	Prev	:null,
	ViewText:null,
	ViewBtn	:null
};

Title.prototype.initialize = function(){
	this.Debug('initialize()');
	var self = this;
	var titlehelpbar = {
			id	:'title',
			data:[
	    	  	{
	    	  		text	:'MOVE',
	    	  		image	:Helpbar.IMAGES_ENUM.LEFT_RIGHT_UP_DOWN,	    	  		
	    	  		onclick	:null
	    	  	},
	    	  	{
	    	  		text	:'SELECT',
	    	  		image	:Helpbar.IMAGES_ENUM.ENTER_KEY,	    	  		
	    	  		onclick	:function(){
	    	  			if(self.IsAnimComplete){
	    	  				self.EnterKeyHandler();
	    	  			}
	    	  		}
	    	  	},
	    	  	{
	    	  		text	:'RETURN',
	    	  		image	:Helpbar.IMAGES_ENUM.RETURN_KEY,	    	  		
	    	  		onclick	:function(){
	    	  			if(self.IsAnimComplete){
	    	  				self.ReturnKeyHandler();
	    	  			}
	    	  		}
	    	  	}    	  	
	    	 ]
	};
	Helpbar.AddObject(titlehelpbar);
	
	this.Html.Logo		= document.getElementById('title-M-btn');
	this.Html.Title		= document.getElementById('title-title');
	this.Html.Desc		= document.getElementById('title-description');
	this.Html.LArrow	= document.getElementById('title-left-arrow');
	this.Html.RArrow	= document.getElementById('title-right-arrow');
	this.Html.Prev		= document.getElementById('title-prevbtn');
	this.Html.Next		= document.getElementById('title-nextbtn'); 
	this.Html.ViewText	= document.getElementById('title-viewbtn');
	$('#title-container').css('opacity', '0');
	
	/*----------------------------- event listener registration ------------------------*/
	this.Html.Logo.addEventListener('click', function(){self.EnterKeyHandler();}, false);	
	this.Html.Logo.addEventListener('mouseover', function(){
		self.ActiveMenu = 0;
		self.HighLight();
	}, false);
	/*----------------------------- event listener registration ------------------------*/
	this.Html.LArrow.addEventListener('click', function(){self.EnterKeyHandler();}, false);	
	this.Html.LArrow.addEventListener('mouseover', function(){
		self.ActiveMenu = 1;
		self.HighLight();
	}, false);
	/*----------------------------- event listener registration ------------------------*/
	this.Html.RArrow.addEventListener('click', function(){self.EnterKeyHandler();}, false);	
	this.Html.RArrow.addEventListener('mouseover', function(){
		self.ActiveMenu = 2;
		self.HighLight();
	}, false);
	/*----------------------------- event listener registration ------------------------*/
	this.Html.Prev.addEventListener('click', function(){self.EnterKeyHandler();}, false);	
	this.Html.Prev.addEventListener('mouseover', function(){
		self.ActiveMenu = 3;
		self.HighLight();
	}, false);
	/*----------------------------- event listener registration ------------------------*/
	this.Html.Next.addEventListener('click', function(){self.EnterKeyHandler();}, false);	
	this.Html.Next.addEventListener('mouseover', function(){
		self.ActiveMenu = 4;
		self.HighLight();
	}, false);
};

Title.prototype.handleShow = function(){
	this.Debug('handleShow()');
	Analytics.track('Masterpieces/GalleryTitle');
	Helpbar.Update('title');
	var self	= this;
	
	this.ActiveMenu	= 2;
	this.HighLight();	
	this.UpdateContent();
	
	this.IsAnimComplete = false;
	$('#title-container').animate({opacity:1}, 500, function(){
		self.IsAnimComplete = true;
	});
};

Title.prototype.handleFocus = function(){
	this.Debug('handleFocus()');
};

Title.prototype.handleHide = function(){
	this.Debug('handleHide()');
};

Title.prototype.handleKeyDown = function(keyCode){
	//this.Debug('handleKeyDown(' + keyCode + ')');
	if(this.IsAnimComplete && !AppMgr.TitleIsShownOnly){
		switch (keyCode){
			case Platform.TvKey.KEY_UP:
				this.Debug('Key Up');
				this.UpKeyHandler();
				break;
			case Platform.TvKey.KEY_DOWN:
				this.Debug('Key Down');
				this.DownKeyHandler();
				break;
			case Platform.TvKey.KEY_LEFT:
				this.Debug('Key Left');
				this.LeftKeyHandler();
				break;
			case Platform.TvKey.KEY_RIGHT:
				this.Debug('Key Right');
				this.RightKeyHandler();
				break;
			case Platform.MouseEvent.SCROLL_UP:
				this.Debug('Mouse Up');
				break;
			case Platform.MouseEvent.SCROLL_DOWN:
				this.Debug('Mouse Down');
				break;
			case Platform.MouseEvent.SCROLL_LEFT:
				this.Debug('Mouse Left');
				break;
			case Platform.MouseEvent.SCROLL_RIGHT:
				this.Debug('Mouse Right');
				break;			
			case Platform.TvKey.KEY_ENTER:
			case Platform.TvKey.KEY_PANEL_ENTER:
				this.EnterKeyHandler();
				break;		
			case Platform.TvKey.KEY_RETURN:
			case Platform.TvKey.KEY_PANEL_RETURN:
				Platform.Block();							// Same as widgetAPI.blockNavigation(event);
				this.ReturnKeyHandler();
				break;
			case Platform.TvKey.KEY_EXIT:
				Platform.Block();
				Platform.WidgetAPI.sendExitEvent();
				break;
		}
	}
	else{		
		switch (keyCode){
		case Platform.TvKey.KEY_RETURN:
			Platform.Block();
			break;
		}
	}
};

Title.prototype.LeftKeyHandler = function(){
	switch(this.ActiveMenu){
	case 1:
		this.EnterKeyHandler();
		break;
	case 2:
		this.ActiveMenu = 1;
		this.HighLight();
		break;
	case 4:
		if(Wall.ActiveIndex != 0){
			this.ActiveMenu = 3;
		}
		this.HighLight();
		break;	
	}
};

Title.prototype.RightKeyHandler = function(){
	switch(this.ActiveMenu){
	case 0:
	case 1:
		this.ActiveMenu = 2;
		this.HighLight();
		break;
	case 2:
		this.EnterKeyHandler();
		break;
	case 3:
		if(Wall.ActiveIndex != Wall.TotalWall - 1){
			this.ActiveMenu = 4;
		}
		this.HighLight();
		break;	
	}
};

Title.prototype.UpKeyHandler = function(){
	switch(this.ActiveMenu){
	case 1:
	case 2:
		this.ActiveMenu = 0;
		this.HighLight();
		break;	
	case 3:
		this.ActiveMenu = 1;
		this.HighLight();
		break;
	case 4:
		this.ActiveMenu = 2;
		this.HighLight();
		break;	
	}
};

Title.prototype.DownKeyHandler = function(){
	switch(this.ActiveMenu){
	case 0:
		this.ActiveMenu = 1;
		this.HighLight();
		break;
	case 1:
		if(Wall.ActiveData.length > 1){
			if(Wall.ActiveIndex > 0){
				this.ActiveMenu = 3;
			}
			else{
				this.ActiveMenu = 4;
			}
		}
		this.HighLight();
		break;
	case 2:
		if(Wall.ActiveData.length > 1){
			if(Wall.ActiveIndex < Wall.TotalWall - 1){
				this.ActiveMenu = 4;
			}
			else{
				this.ActiveMenu = 3;
			}
		}
		this.HighLight();
		break;	
	}
};

Title.prototype.EnterKeyHandler = function(){
	var self 	= this;
	var currBG	= '';
	var clickBG	= '';
	var curElm	= null;
	
	switch(this.ActiveMenu){
	case 0:
		currBG	= 'url(resource/images/title/M-sel.png)';
		clickBG	= 'url(resource/images/title/M-click.png)';
		curElm	= self.Html.Logo;
		break;
	case 1:
		currBG	= 'url(resource/images/title/arrow-white-sel.png)';
		clickBG	= 'url(resource/images/title/arrow-white-click.png)';
		curElm	= self.Html.LArrow;
		break;
	case 2:
		currBG	= 'url(resource/images/title/arrow-white-sel.png)';
		clickBG	= 'url(resource/images/title/arrow-white-click.png)';
		curElm	= self.Html.RArrow;
		break;
	case 3:
		currBG	= 'url(resource/images/title/prev-sel.png)';
		clickBG	= 'url(resource/images/title/prev-click.png)';
		curElm	= self.Html.Prev;
		break;
	case 4:
		currBG	= 'url(resource/images/title/next-sel.png)';
		clickBG	= 'url(resource/images/title/next-click.png)';
		curElm	= self.Html.Next;
		break;	
	}
	
	curElm.style.backgroundImage = clickBG;
	setTimeout(function(){
		curElm.style.backgroundImage = currBG;
		switch(self.ActiveMenu){
		case 0:
			self.ReturnKeyHandler();
			break;
		case 1:
			if(Wall.ActiveIndex > 0){
				do{
					Wall.ActiveIndex--;
				}
				while(Wall.ActiveData[Wall.ActiveIndex].Arts.length == 0);						
			}
			else{
				Wall.ActiveIndex = Wall.TotalWall - 1;
			}
			Wall.SelectedImage = Wall.ActiveData[Wall.ActiveIndex].Arts.length - 1;
			
			self.IsAnimComplete = false;
			$('#title-container').animate({opacity:0}, 500, function(){			
				self.IsAnimComplete = true;
				AppMgr.BeforeDrawer = 'Title';
				SceneManager.Show('Drawer');
				SceneManager.Focus('Drawer');
				SceneManager.Hide('Title');
			});
			break;
		case 2:
			Wall.SelectedImage	= 0;
			self.IsAnimComplete = false;
			$('#title-container').animate({opacity:0}, 500, function(){			
				self.IsAnimComplete = true;
				AppMgr.BeforeDrawer = 'Title';
				SceneManager.Show('Drawer');
				SceneManager.Focus('Drawer');
				SceneManager.Hide('Title');
			});
			break;
		case 3:
			if(Wall.ActiveIndex > 0){
				Wall.ActiveIndex--;
				self.IsAnimComplete = false;
				$('#title-container').animate({opacity:0}, 500, function(){			
					self.UpdateContent();
					if(Wall.ActiveIndex == 0){
						self.ActiveMenu = 4;
						self.HighLight();
					}
					$('#title-container').animate({opacity:1}, 500, function(){
						self.IsAnimComplete = true;
					});
					
				});
			}			
			break;
		case 4:
			self.GoNextGallery();		
			break;
		}
	},200);	
};

Title.prototype.ReturnKeyHandler = function(){
	var self = this;
	this.IsAnimComplete = false;
	$('#title-container').animate({opacity:0}, 500, function(){
		self.IsAnimComplete = true;
		SceneManager.Show('Home');
		SceneManager.Focus('Home');
		SceneManager.Hide('Title');
	});
};

Title.prototype.GoNextGallery = function(){
	var self = this;
	if(Wall.ActiveIndex < Wall.TotalWall - 1){
		Wall.ActiveIndex++;
		self.IsAnimComplete = false;
		$('#title-container').animate({opacity:0}, 500, function(){			
			self.UpdateContent();
			if(Wall.ActiveIndex == Wall.TotalWall - 1){
				self.ActiveMenu = 3;
				self.HighLight();
			}
			$('#title-container').animate({opacity:1}, 500, function(){
				self.IsAnimComplete = true;
			});
		});
	}
};

Title.prototype.UpdateContent = function(){
	if(Wall.WallType == 'masterpieces'){
		Wall.ActiveData		= Masterpieces;
		Wall.TotalWall		= Wall.ActiveData.length;
	}	
	
	$('#title-container').css('background-image', 'url(resource/images/title/biru-bg.png)');	
	Platform.WidgetAPI.putInnerHTML(this.Html.Title, Wall.ActiveData[Wall.ActiveIndex].Title);
	Platform.WidgetAPI.putInnerHTML(this.Html.Desc, Wall.ActiveData[Wall.ActiveIndex].Desc);
	
	if(AppMgr.TitleIsShownOnly){
		this.Html.Logo.style.display		= 'none';
		this.Html.ViewText.style.display	= 'none';
		this.Html.LArrow.style.display		= 'none';
		this.Html.RArrow.style.display		= 'none';
		this.Html.Prev.style.display		= 'none';
		this.Html.Next.style.display		= 'none';
		
		Helpbar.Hide();
	}
	else{
		this.Html.Logo.style.display		= 'block';
		this.Html.ViewText.style.display	= 'block';
		this.Html.LArrow.style.display		= 'block';
		this.Html.RArrow.style.display		= 'block';
		
		if(Wall.ActiveData.length <= 1){
			this.Html.Prev.style.display	= 'none';
			this.Html.Next.style.display	= 'none';
		}
		else if(Wall.ActiveIndex == 0){
			this.Html.Prev.style.display	= 'none';
			this.Html.Next.style.display	= 'block';
		}
		else if(Wall.ActiveIndex == Wall.TotalWall - 1){
			this.Html.Prev.style.display	= 'block';
			this.Html.Next.style.display	= 'none';
		}
		else{
			this.Html.Prev.style.display	= 'block';
			this.Html.Next.style.display	= 'block';
		}
	}	
};

Title.prototype.HighLight = function(){
	this.Html.Logo.style.backgroundImage	= 'url(resource/images/title/M.png)';
	this.Html.LArrow.style.backgroundImage	= 'url(resource/images/title/arrow-white.png)';	
	this.Html.RArrow.style.backgroundImage	= 'url(resource/images/title/arrow-white.png)';	
	this.Html.Prev.style.backgroundImage	= 'url(resource/images/title/prev.png)';
	this.Html.Next.style.backgroundImage	= 'url(resource/images/title/next.png)';
	
	switch(this.ActiveMenu){
	case 0:
		this.Html.Logo.style.backgroundImage	= 'url(resource/images/title/M-sel.png)';
		break;
	case 1:
		this.Html.LArrow.style.backgroundImage	= 'url(resource/images/title/arrow-white-sel.png)';
		break;
	case 2:
		this.Html.RArrow.style.backgroundImage	= 'url(resource/images/title/arrow-white-sel.png)';
		break;	
	case 3:
		this.Html.Prev.style.backgroundImage	= 'url(resource/images/title/prev-sel.png)';
		break;
	case 4:
		this.Html.Next.style.backgroundImage	= 'url(resource/images/title/next-sel.png)';
		break;	
	}
};

Title.prototype.HideOnAutoplay = function(callback){
	var self = this;
	self.IsAnimComplete = false;
	$('#title-container').animate({opacity:0}, 500, function(){			
		self.IsAnimComplete = true;
		if(callback){
			callback();
		}
	});
};

Title.prototype.Debug = function(message){
	Platform.Debug('[SCENE >> Title] ' + message);
};