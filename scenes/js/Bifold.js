/* File: Bifold.js
 *
 * Description:
 * Scenes javascript file for Bifold
 *
 * Author:
 * I Made Krisna (made.krisna@samsung.com)
 *
 * Changelog:
 * - 2012-12-26 Initial Code (made.krisna)
 */

var Active_Container = 0;
var BIFOLD_SCROLL_TOP;
 
function Bifold(){
}

Bifold.prototype.IsAnimComplete = true;
Bifold.prototype.IsButtonHidden	= false;
Bifold.prototype.IsUserPressRmt	= false;
Bifold.prototype.CheckTimer		= null;
Bifold.prototype.SlideTimer		= null;
Bifold.prototype.TitleTimeout	= null;
Bifold.prototype.SlidePeriod	= 7;
Bifold.prototype.RandomImageId	= 0;

Bifold.prototype.MousePos		= {
	X		:-1,
	Y		:-1,
	PrevX	:-1,
	PrexY	:-1
};

Bifold.prototype.Html			= {
	InfoButton	: null,
	BackButton	: null,
	ImgContainer: null
};

Bifold.prototype.initialize = function(){
	this.Debug('initialize()');
	var self = this;
	
	$('#bifold-container').css('opacity', '0.0');
	
	self.Html.InfoButton	= document.getElementById('bifold-infobtn');
	self.Html.BackButton	= document.getElementById('bifold-backbtn');
	self.Html.ImgContainer	= document.getElementById('bifold-image-container');
	
	
	self.Html.InfoButton.addEventListener('mouseover', function(){
		self.Html.InfoButton.style.backgroundImage = 'url(resource/images/bifold/gallery-detail-selborder.png)';
	}, false);
	self.Html.InfoButton.addEventListener('mouseout', function(){
		self.Html.InfoButton.style.backgroundImage = 'none';
	}, false);
	self.Html.InfoButton.addEventListener('click', function(){
		self.Html.InfoButton.style.backgroundImage = 'url(resource/images/bifold/gallery-detail-click.png)';
		setTimeout(function(){
			self.Html.InfoButton.style.backgroundImage = 'url(resource/images/bifold/gallery-detail-selborder.png)';
			self.InfoKeyHandler();
		}, 200);
	}, false);
	
	self.Html.BackButton.addEventListener('mouseover', function(){
		self.Html.BackButton.style.backgroundImage = 'url(resource/images/bifold/gallery-detail-selborder.png)';
	}, false);
	self.Html.BackButton.addEventListener('mouseout', function(){
		self.Html.BackButton.style.backgroundImage = 'none';
	}, false);
	self.Html.BackButton.addEventListener('click', function(){
		self.Html.BackButton.style.backgroundImage = 'url(resource/images/bifold/gallery-detail-click.png)';
		setTimeout(function(){
			self.Html.BackButton.style.backgroundImage = 'url(resource/images/bifold/gallery-detail-selborder.png)';
			self.ReturnKeyHandler();
		}, 200);
	}, false);
	
	self.Html.ImgContainer.addEventListener('mousemove', function(event){
		self.MousePos.X	= event.layerX;
		self.MousePos.Y	= event.layerY;
	}, false);
	self.Html.ImgContainer.addEventListener('click', function(){
		if(self.IsAnimComplete){
			self.RightKeyHandler();
		}		
	}, false);
};

Bifold.prototype.handleShow = function(){
	this.Debug('handleShow()');
	Analytics.track('Masterpieces/Fullscreen');
	Helpbar.Hide();
	Active_Container = 0;
	this.IsUserPressRmt	= false;
	this.MousePos.X		= -1;
	this.MousePos.Y		= -1;
	this.MousePos.PrevX	= -1;
	this.MousePos.PrexY	= -1;
	this.loadContent();
};

Bifold.prototype.handleFocus = function(){
	this.Debug('handleFocus()');	
};

Bifold.prototype.handleHide = function(){
	this.Debug('handleHide()');
	this.StopSlideShow();
	this.StopCheckMovement();
	clearTimeout(this.TitleTimeout);
	Autoplay.IsSet = false;
};

Bifold.prototype.scrollContent = function(type){
	var offsetHeight = document.getElementById('bifold-artistdesc').offsetHeight;
	var scrollHeight = document.getElementById('bifold-artistdesc').scrollHeight;
	
	if (offsetHeight < scrollHeight){
		
		if (type == 1){
			document.getElementById('bifold-artistdesc').scrollTop -= 20;
		}
		else if (type == 2){
			document.getElementById('bifold-artistdesc').scrollTop += 20;
		}
		
		var ratio	= document.getElementById('bifold-artistdesc').scrollTop / (scrollHeight - offsetHeight);
		var top		= Math.ceil((offsetHeight - 50) * ratio);
		$('#bifold-scroll-bead').css('top', top + 'px');
		$('#bifold-scroll-back').css('display', 'block');
	}
	else{
		$('#bifold-scroll-back').css('display', 'none');
	}
};

Bifold.prototype.loadContent = function(){
	var self = this;
	if(Autoplay.IsSet&& Autoplay.IsRandom){
		self.Randomize();
	}	
	self.updateContent();
	setTimeout(function(){	
		self.IsAnimComplete = false;
		$('#bifold-container').animate({opacity:1}, 500, function(){
			$('#bifold-detailed-container').animate({left:'1200px'}, 500, function(){				
				self.IsButtonHidden = false;
				self.Html.InfoButton.style.display	= 'block';
				self.StartCheckMovement();
				if(Autoplay.IsSet){
					self.PlayKeyHandler();
				}
				self.IsAnimComplete = true;
			});
		});		
	},500);
};

Bifold.prototype.updateContent = function(){
	switch(Wall.SelectedImage){
	case 0:
		$('#bifold-image-0').css('background-image', 'url(' + Wall.ActiveData[Wall.ActiveIndex].Arts[Wall.ActiveData[Wall.ActiveIndex].Arts.length-1].Image + ')');
		$('#bifold-image-1').css('background-image', 'url(' + Wall.ActiveData[Wall.ActiveIndex].Arts[Wall.SelectedImage].Image + ')');
		$('#bifold-image-2').css('background-image', 'url(' + Wall.ActiveData[Wall.ActiveIndex].Arts[Wall.SelectedImage+1].Image + ')');
		break;
	case (Wall.ActiveData[Wall.ActiveIndex].Arts.length-1):
		$('#bifold-image-0').css('background-image', 'url(' + Wall.ActiveData[Wall.ActiveIndex].Arts[Wall.SelectedImage-1].Image + ')');
		$('#bifold-image-1').css('background-image', 'url(' + Wall.ActiveData[Wall.ActiveIndex].Arts[Wall.SelectedImage].Image + ')');
		$('#bifold-image-2').css('background-image', 'url(' + Wall.ActiveData[Wall.ActiveIndex].Arts[0].Image + ')');
		break;
	default:
		$('#bifold-image-0').css('background-image', 'url(' + Wall.ActiveData[Wall.ActiveIndex].Arts[Wall.SelectedImage-1].Image + ')');
		$('#bifold-image-1').css('background-image', 'url(' + Wall.ActiveData[Wall.ActiveIndex].Arts[Wall.SelectedImage].Image + ')');
		$('#bifold-image-2').css('background-image', 'url(' + Wall.ActiveData[Wall.ActiveIndex].Arts[Wall.SelectedImage+1].Image + ')');
		break;
	}
	this.updateDetails();	
};

Bifold.prototype.updateRandomContent = function(){
	$('#bifold-image-0').css('background-image', 'url(' + Wall.ActiveData[Wall.ActiveIndex].Arts[Wall.ActiveData[Wall.ActiveIndex].Arts.length-1].Image + ')');
	$('#bifold-image-1').css('background-image', 'url(' + Wall.ActiveData[Wall.ActiveIndex].Arts[Wall.SelectedImage].Image + ')');
	$('#bifold-image-2').css('background-image', 'url(' + Wall.ActiveData[Wall.ActiveIndex].Arts[Wall.SelectedImage+1].Image + ')');	
	this.updateDetails();	
};

Bifold.prototype.updateDetails = function(){
	$('#bifold-artistphoto').css('background-image', 'url(' + Wall.ActiveData[Wall.ActiveIndex].Arts[Wall.SelectedImage].Artist.Ava + ')');
	
	Platform.WidgetAPI.putInnerHTML(document.getElementById('bifold-title'), Wall.ActiveData[Wall.ActiveIndex].Arts[Wall.SelectedImage].Title);
	Platform.WidgetAPI.putInnerHTML(document.getElementById('bifold-artistname'), Wall.ActiveData[Wall.ActiveIndex].Arts[Wall.SelectedImage].Artist.Name);
	Platform.WidgetAPI.putInnerHTML(document.getElementById('bifold-artistedu'), Wall.ActiveData[Wall.ActiveIndex].Arts[Wall.SelectedImage].Artist.Edu);
	Platform.WidgetAPI.putInnerHTML(document.getElementById('bifold-artistloc'), Wall.ActiveData[Wall.ActiveIndex].Arts[Wall.SelectedImage].Artist.Loc);
	Platform.WidgetAPI.putInnerHTML(document.getElementById('art-desc'), '"' + Wall.ActiveData[Wall.ActiveIndex].Arts[Wall.SelectedImage].Dedication + '"');
	Platform.WidgetAPI.putInnerHTML(document.getElementById('artist-bio-content'), Wall.ActiveData[Wall.ActiveIndex].Arts[Wall.SelectedImage].Artist.Bio);
		
	if(!Wall.ActiveData[Wall.ActiveIndex].Arts[Wall.SelectedImage].Artist.Edu){
		$('#bifold-edu').css('opacity', '0.0');
	}
	else{
		$('#bifold-edu').css('opacity', '1.0');
	}
	
	if(!Wall.ActiveData[Wall.ActiveIndex].Arts[Wall.SelectedImage].Artist.Loc){
		$('#bifold-loc').css('opacity', '0.0');
	}
	else{
		$('#bifold-loc').css('opacity', '1.0');
	}
	
	if(!Wall.ActiveData[Wall.ActiveIndex].Arts[Wall.SelectedImage].Artist.Bio){
		$('#artist-bio-title').css('opacity', '0.0');
		$('#artist-bio-content').css('opacity', '0.0');
	}
	else{
		$('#artist-bio-title').css('opacity', '1.0');
		$('#artist-bio-content').css('opacity', '1.0');
	}
	
	setTimeout(function(){
		var topPos	 = document.getElementById('bifold-title').scrollHeight + 10;
		topPos		+= document.getElementById('bifold-artistname').scrollHeight + 10;
		topPos		+= document.getElementById('bifold-edu').scrollHeight + 10;
		topPos		+= document.getElementById('bifold-loc').scrollHeight + 20;	
		$('#bifold-artistdesc').css('height', (700 - topPos) + 'px');
		$('#bifold-scroll-back').css('top', topPos + 'px');
		$('#bifold-scroll-back').css('height', (700 - topPos) + 'px');
		$('#bifold-scroll-bead').css('top', '0px');
		
		var offsetHeight = document.getElementById('bifold-artistdesc').offsetHeight;
		var scrollHeight = document.getElementById('bifold-artistdesc').scrollHeight;
		
		document.getElementById('bifold-artistdesc').scrollTop	= 0;
		
		if (offsetHeight < scrollHeight){
			$('#bifold-scroll-back').css('display', 'block');
		}
		else{
			$('#bifold-scroll-back').css('display', 'none');
		}
	},500);	
};

Bifold.prototype.unloadContent = function(){
	var self = this;
	
	SceneManager.Show(AppMgr.PrevScene);	
	self.Html.InfoButton.style.display	= 'none';
	self.IsAnimComplete = false;
	$('#bifold-detailed-container').animate({left:'1245px'}, 500, function(){
		$('#bifold-container').animate({opacity:0}, 500, function(){		
			if(Autoplay.IsSet){
				SceneManager.Focus('Home');
			}
			else{
				SceneManager.Focus('Drawer');
			}
			SceneManager.Hide('Bifold');
			self.IsAnimComplete = true;
		});
	});	
};

Bifold.prototype.handleKeyDown = function(keyCode){
	//this.Debug('handleKeyDown(' + keyCode + ')');
	var self = this;	
	if(self.IsAnimComplete){
		switch (keyCode){
			case Platform.TvKey.KEY_UP:
				this.Debug('Key Up');
				if(Active_Container == 1){
					this.scrollContent(1);
				}
				break;
			case Platform.TvKey.KEY_DOWN:
				this.Debug('Key Down');
				if(Active_Container == 1){
					this.scrollContent(2);
				}
				break;
			case Platform.TvKey.KEY_RW:
			case Platform.TvKey.KEY_LEFT:
				this.Debug('Key Left');
				this.LeftKeyHandler();
				break;
			case Platform.TvKey.KEY_FF:
				case Platform.TvKey.KEY_RIGHT:
				this.Debug('Key Right');
				this.RightKeyHandler();
				break;
			case Platform.TvKey.KEY_PLAY:
				this.Debug('Key Play');
				this.PlayKeyHandler();
				break;			
			case Platform.TvKey.KEY_STOP:
				this.Debug('Key Stop');
				this.StopSlideShow();
				break;			
			case Platform.TvKey.KEY_INFO:
			case Platform.TvKey.KEY_ENTER:
			case Platform.TvKey.KEY_PANEL_ENTER:
				this.Debug('Key Info/Enter');
				this.InfoKeyHandler();
				break;		
			case Platform.TvKey.KEY_RETURN:
			case Platform.TvKey.KEY_PANEL_RETURN:
				Platform.Block();							// Same as widgetAPI.blockNavigation(event);
				this.ReturnKeyHandler();
				break;
			case Platform.TvKey.KEY_EXIT:
				Platform.Block();
				Platform.WidgetAPI.sendExitEvent();
				break;
		}
	}
	else{
		Platform.Block();
		switch (keyCode){
		case Platform.TvKey.KEY_EXIT:
			this.Debug('Key Exit');
			Platform.WidgetAPI.sendExitEvent();
			break;
		}
	}
};

Bifold.prototype.ResetRandomFlag = function(){	
	for(var i = 0; i < Wall.ActiveData.length; i++){
		for(var j = 0; j < Wall.ActiveData[i].Arts.length; j++){
			Wall.ActiveData[i].Arts[j].IsOpened = false;
		}
	}
	AppMgr.TotalOpenedImage	= 0;
};

Bifold.prototype.Randomize = function(){
	var self = this;
	var maxwall			= Wall.ActiveData.length - 1;
	Wall.ActiveIndex	= Math.round(Math.random()*maxwall);
	if(Wall.ActiveData[Wall.ActiveIndex].Arts.length){
		var maxidx			= Wall.ActiveData[Wall.ActiveIndex].Arts.length - 1;
		Wall.SelectedImage	= Math.round(Math.random()*maxidx);
		
		/* Check if all images already opened*/
		if(AppMgr.TotalOpenedImage == AppMgr.TotalCuratedImages){
			self.Debug('Randomize >>--> Reset Random Flag');
			self.ResetRandomFlag();
		}
		/* Check it was opened or not*/
		if(Wall.ActiveData[Wall.ActiveIndex].Arts[Wall.SelectedImage].IsOpened){
			self.Debug('Randomize >>--> Image was opened');
			self.Randomize();		
		}
		else{
			self.Debug('Randomize >>--> Wall = ' + Wall.ActiveData[Wall.ActiveIndex].Title + ', Art = ' + Wall.SelectedImage);
			Wall.ActiveData[Wall.ActiveIndex].Arts[Wall.SelectedImage].IsOpened = true;
			AppMgr.TotalOpenedImage++;
			self.Debug('Total Opened Image = ' + AppMgr.TotalOpenedImage);
		}
		/* End of check*/
	}
	else{
		self.Debug('Randomize >>--> Active wall is empty');
		self.Randomize();		
	}		
};

Bifold.prototype.RandomSlide = function(direction){
	//this.Debug('Sliding...');
	var self 	= this;
	var leftPos	= null;
	
	self.RandomImageId = Wall.SelectedImage;	
	
	if(direction == -1){ 		//left
		$('#bifold-image-2').css('background-image', 'url(' + Wall.ActiveData[Wall.ActiveIndex].Arts[self.RandomImageId].Image + ')');
		
		self.IsAnimComplete = false;
		$('#bifold-image-container').animate({left:'-2560px'}, 1000, function(){
			$('#bifold-image-1').css('background-image', 'url(' + Wall.ActiveData[Wall.ActiveIndex].Arts[self.RandomImageId].Image + ')');
			$('#bifold-image-container').css('left', '-1280px');
			self.updateDetails();
			self.Debug('Animation Completed..');
			self.IsAnimComplete = true;
		});
	}
	else if(direction == 1){	//right
		$('#bifold-image-0').css('background-image', 'url(' + Wall.ActiveData[Wall.ActiveIndex].Arts[self.RandomImageId].Image + ')');
		
		self.IsAnimComplete = false;
		$('#bifold-image-container').animate({left:'0px'}, 1000, function(){
			$('#bifold-image-1').css('background-image', 'url(' + Wall.ActiveData[Wall.ActiveIndex].Arts[self.RandomImageId].Image + ')');
			$('#bifold-image-container').css('left', '-1280px');
			self.updateDetails();
			self.Debug('Animation Completed..');
			self.IsAnimComplete = true;
		});
	}
};

Bifold.prototype.Slide = function(direction){
	//this.Debug('Sliding...');
	var self 	= this;
	var leftPos	= null;
	if(direction == -1){ 		//left
		leftPos = '-2560px';
	}
	else if(direction == 1){	//right
		leftPos = '0px';
	}
	self.IsAnimComplete = false;
	$('#bifold-image-container').animate({left:leftPos}, 1000, function(){		
		self.updateContent();
		$('#bifold-image-container').css('left', '-1280px');
		self.IsAnimComplete = true;
	});
};

Bifold.prototype.RightKeyHandler = function(){
	if(Autoplay.IsRandom){
		this.Randomize();
		this.RandomSlide(-1);
	}
	else{
		if(Wall.SelectedImage < Wall.ActiveData[Wall.ActiveIndex].Arts.length - 1){
			Wall.SelectedImage++;
			this.Slide(-1);
		}
		else{
			this.ShowTitlePageThenBack(2);
		}
	}
};

Bifold.prototype.LeftKeyHandler = function(){
	if(Autoplay.IsRandom){
		this.Randomize();
		this.RandomSlide(1);
	}
	else{
		if(Wall.SelectedImage > 0){
			Wall.SelectedImage--;
			this.Slide(1);
		}
		else{
			this.ShowTitlePageThenBack(1);
		}
	}
};

Bifold.prototype.ReturnKeyHandler = function(){	
	if(Active_Container == 1){
		var self = this;
		self.IsAnimComplete = false;
		$('#bifold-detailed-container').animate({left:'1245px'}, 1000, function(){			
			self.IsButtonHidden = true;
			self.StartCheckMovement();
			Active_Container = 0;
			self.IsAnimComplete = true;
		});
	}
	else{
		this.unloadContent();
	}
};

Bifold.prototype.InfoKeyHandler = function(){
	if(Active_Container == 0){
		var self = this;
		self.IsAnimComplete = false;
		$('#bifold-detailed-container').animate({left:'826px'}, 1000, function(){			
			self.IsButtonHidden = false;
			self.StopCheckMovement();
			Active_Container = 1;
			self.IsAnimComplete = true;
		});
		
	}
	else{
		this.ReturnKeyHandler();
	}
};

Bifold.prototype.PlayKeyHandler = function(){
	if(!this.SlideTimer){
		Autoplay.IsSet = true;
		if(!Autoplay.IsRandom){
			this.StartSlideShow();
		}
		else{
			this.StartRandomSlideShow();
		}
	}
};

Bifold.prototype.StartSlideShow = function(){
	var self = this;
	Autoplay.IsSet = true;
	self.SlideTimer = setInterval(function(){
		if(self.IsAnimComplete){
			if(Wall.SelectedImage < Wall.ActiveData[Wall.ActiveIndex].Arts.length - 1){
				Wall.SelectedImage++;
				self.Slide(-1);
			}
			else if(Wall.ActiveIndex < Wall.TotalWall - 1){
				do{
					Wall.ActiveIndex++;
				}
				while(Wall.ActiveData[Wall.ActiveIndex].Arts.length == 0);
				Wall.SelectedImage	= 0;
				self.ShowTitlePageThenBack();			
			}
			else{
				Wall.ActiveIndex	= 0;			
				Wall.SelectedImage	= 0;
				self.ShowTitlePageThenBack();
			}
		}
	}, parseInt(self.SlidePeriod)*1000);
	Platform.SetScreenSaver(false);
};

Bifold.prototype.StartRandomSlideShow = function(){
	var self		= this;	
	Autoplay.IsSet	= true;
	self.SlideTimer = setInterval(function(){
		if(self.IsAnimComplete){
			self.Randomize();
			self.RandomSlide(-1);
		}
	}, parseInt(self.SlidePeriod)*1000);
	Platform.SetScreenSaver(false);
};

Bifold.prototype.StopSlideShow = function(){
	//Autoplay.IsSet = false;
	clearInterval(this.SlideTimer);
	this.SlideTimer = null;
	Platform.SetScreenSaver(true);
};

Bifold.prototype.ShowTitlePageThenBack = function(direction){
	var self = this;
	if(Autoplay.IsSet){
		self.StopSlideShow();
	}
	
	if(direction == 2){	//right
		if(Wall.ActiveIndex < Wall.TotalWall - 1){
			do{
				Wall.ActiveIndex++;
			}
			while(Wall.ActiveData[Wall.ActiveIndex].Arts.length == 0);		
		}
		else{
			Wall.ActiveIndex	= 0;
		}
		Wall.SelectedImage	= 0;
	}
	
	self.IsAnimComplete = false;
	$('#bifold-container').animate({opacity:0},500, function(){		
		AppMgr.TitleIsShownOnly	= true;
		SceneManager.Show('Title');
		SceneManager.Focus('Title');
		if(direction == 1){	//left
			if(Wall.ActiveIndex > 0){
				do{
					Wall.ActiveIndex--;
				}
				while(Wall.ActiveData[Wall.ActiveIndex].Arts.length == 0);		
			}
			else{
				Wall.ActiveIndex	= Wall.TotalWall - 1;
			}
			Wall.SelectedImage	= Wall.ActiveData[Wall.ActiveIndex].Arts.length - 1;
		}
	});
	
	self.TitleTimeout = setTimeout(function(){		
		$('#bifold-image-container').css('left','-1280px');
		SceneManager.Collection['Title'].HideOnAutoplay(function(){
			$('#bifold-container').animate({opacity:1},500, function(){				
				SceneManager.Focus('Bifold');
				SceneManager.Hide('Title');
				self.IsAnimComplete = true;
			});
		});		
		Helpbar.Hide();
		self.updateContent();
		if(Autoplay.IsSet){
			self.StartSlideShow(self.SlidePeriod);
		}
		clearTimeout(self.TitleTimeout);
	},7000);
};

Bifold.prototype.StartCheckMovement= function(){
	var self = this;
	if(!self.CheckTimer){
		self.CheckTimer = setInterval(function(){
			if(self.MousePos.PrevX == self.MousePos.X && self.MousePos.PrevY == self.MousePos.Y && !self.IsUserPressRmt){
				self.HideButton();
			}
			else{
				self.ShowButton();
			}
			self.MousePos.PrevX = self.MousePos.X;
			self.MousePos.PrevY = self.MousePos.Y;
		},3000);		
	}
};

Bifold.prototype.StopCheckMovement= function(){
	var self = this;
	clearInterval(self.CheckTimer);
	self.CheckTimer = null;
};

Bifold.prototype.ShowButton = function(){
	if(this.IsButtonHidden && this.IsAnimComplete){
		var self = this;
		self.IsAnimComplete = false;
		$('#bifold-detailed-container').animate({left:'1200px'}, 500, function(){
			self.IsAnimComplete = true;
		});
		self.IsButtonHidden = false;
	}
};

Bifold.prototype.HideButton = function(){
	if(!this.IsButtonHidden && this.IsAnimComplete && Active_Container == 0){
		var self = this;
		self.IsAnimComplete = false;
		$('#bifold-detailed-container').animate({left:'1245px'}, 500, function(){
			self.IsAnimComplete = true;
		});
		self.IsButtonHidden = true;
	}
};

Bifold.prototype.Debug = function(message){
	Platform.Debug('[SCENE >> Bifold] ' + message, 'cyan');
};
