var Trending		= [
	{
		Type		: 'trending',
		Title		: 'TRENDING GALLERY',
		Desc		: 'Find beautiful artwork that is currently trending in the Masterpieces community.',
		Arts		: new Array()
	}
];
var Masterpieces	= new Array();

var Wall			= {
	ActiveData		: null,
	WallType		: 'trending',
	TotalWall		: 0,
	ActiveIndex		: 0,
	TotalImage		: 0,
	SelectedImage	: 0
};

var Autoplay		= {
	IsSet			: false,
	IsRandom		: false
};

var AppMgr			= {
	TrendingAjax	: null,
	MasterAjax		: null,
	PrevScene		: 'Home',
	TrendingURL		: 'http://www.masterpiecesart.com/api/v1/arts/trending?page=',
	MasterURL		: 'http://www.masterpiecesart.com/api/v1/masterpieces?page=',
	TrendingPage	: 1,
	TrendingMaxPage	: 1,
	MasterPage		: 1,
	MasterMaxPage	: 1,
	BeforeDrawer	: 'Title',
	TitleIsShownOnly: false,
	TotalOpenedImage: 0,
	TotalCuratedImages : 0,
	NetworkIsOn		: false,
	IsGettingGallery: false
};

AppMgr.AbortTrendingRequest = function(){
	/* abort ajax */
	AppMgr.Debug('Abort Trending Request');
	if (AppMgr.TrendingAjax !== null){
		try{
			AppMgr.TrendingAjax.abort();
		}
		catch (e){
			AppMgr.Debug('Exception on Aborting Ajax: ' + e);
		}		
	}
	AppMgr.TrendingAjax = null;	
	/* end of abort ajax*/
};

AppMgr.AbortMasterRequest = function(){
	/* abort ajax */
	AppMgr.Debug('Abort Master Request');
	if (AppMgr.MasterAjax !== null){
		try{
			AppMgr.MasterAjax.abort();
		}
		catch (e){
			AppMgr.Debug('Exception on Aborting Ajax: ' + e);
		}		
	}
	AppMgr.MasterAjax = null;
	/* end of abort ajax*/
};

AppMgr.GetTrending = function(page, callback){	
	AppMgr.Debug('Getting Trending Page ' + AppMgr.TrendingPage + ' of ' + AppMgr.TrendingMaxPage);
	if (Platform.CheckNetwork() === false){		
		callback(null, 408, 'No Network Connection');
		return;
	}
	
	var code 	= 0;
	var text 	= 'Unknown Error';
	var json	= null;
	
	AppMgr.AbortTrendingRequest();
	
	AppMgr.TrendingAjax = $.ajax({
		url			: AppMgr.TrendingURL + page,
		timeout		: 30000,
		type		: 'GET',
		dataType	: 'json',
		complete	: function(xhr, status){
			if (status === 'timeout'){
				code = 408;
				text = 'Connection Timeout';
			}
			else{
				code = xhr.status;
				text = xhr.statusText;
			}
			setTimeout(function(){
				AppMgr.TrendingAjax = null;	
				callback(json, code, text);
			}, 1000);

		},
		success		: function(data){			
			if (data){
				json = data;
			}
		}
	});
};

AppMgr.GetMasterpieces = function(page, callback){	
	AppMgr.Debug('Getting Master Page ' + AppMgr.MasterPage + ' of ' + AppMgr.MasterMaxPage);
	if (Platform.CheckNetwork() === false){		
		callback(null, 408, 'No Network Connection');
		return;
	}
	
	var code 	= 0;
	var text 	= 'Unknown Error';
	var json	= null;
	
	AppMgr.AbortMasterRequest();
	
	AppMgr.MasterAjax = $.ajax({
		url			: AppMgr.MasterURL + page,
		timeout		: 30000,
		type		: 'GET',
		dataType	: 'json',
		complete	: function(xhr, status){
			if (status === 'timeout'){
				code = 408;
				text = 'Connection Timeout';
			}
			else{
				code = xhr.status;
				text = xhr.statusText;
			}
			setTimeout(function(){
				AppMgr.MasterAjax = null;		
				callback(json, code, text);				
			}, 1000);

		},
		success		: function(data){			
			if (data){
				json = data;
			}
		}
	});
};

AppMgr.RetrieveTrending = function(json, code, text){
	//AppMgr.Debug('Retrieving Trending Data...');
	if(json && json.arts.length){
		var totalArts	= json.arts.length;		
		
		AppMgr.TrendingMaxPage	= json.total_pages;
				
		for(var m=0; m < totalArts; m++){
			if(json.arts[m].format_type == 'image'){
				var artcontent 			= new Object();
				artcontent.Image		= json.arts[m].image_original_medium;
				artcontent.Title		= json.arts[m].title;
				artcontent.Dedication	= json.arts[m].dedication;
				artcontent.Artist		= new Object();
				
				artcontent.Artist.Name	= json.arts[m].artist.name;
				artcontent.Artist.Edu	= json.arts[m].artist.education;
				artcontent.Artist.Loc	= json.arts[m].artist.location;
				artcontent.Artist.Bio	= json.arts[m].artist.bio;	
				artcontent.Artist.Ava	= json.arts[m].artist.avatar;
				
				Trending[0].Arts.push(artcontent);						
			}
		}		
		AppMgr.TrendingPage++;
		if(Wall.WallType == 'trending'){
			Wall.ActiveData	= Trending;
			Wall.TotalWall	= Wall.ActiveData.length;
		}
		//AppMgr.Debug($.toJSON(Trending));
	}
	else{
		AppMgr.Debug('Data is empty...');
		//error notification is here
	}
};

AppMgr.RetrieveMasterpieces = function(json, code, text){
	//AppMgr.Debug('Retrieving Masterpieces Data...');	
	if(json && json.sections.length){
		AppMgr.MasterMaxPage	= json.total_pages;
		for(var n=0; n < json.sections.length; n++){
			if(json.sections[n].arts.length){
				var totalArts = json.sections[n].arts.length;
				var wall		= new Object();
				
				wall.Type		= 'masterpieces';
				wall.Title		= json.sections[n].title.toUpperCase();
				wall.Desc		= json.sections[n].description;
				wall.Arts		= new Array();				
				
				for(var m=0; m < totalArts; m++){
					if(json.sections[n].arts[m].format_type == 'image'){
						var artcontent 			= new Object();
						artcontent.IsOpened		= false; 
						artcontent.Image		= json.sections[n].arts[m].image_original_medium;
						artcontent.Title		= json.sections[n].arts[m].title;
						artcontent.Dedication	= json.sections[n].arts[m].dedication;
						artcontent.Artist		= new Object();
						
						artcontent.Artist.Name	= json.sections[n].arts[m].artist.name;
						artcontent.Artist.Edu	= json.sections[n].arts[m].artist.education;
						artcontent.Artist.Loc	= json.sections[n].arts[m].artist.location;
						artcontent.Artist.Bio	= json.sections[n].arts[m].artist.bio;	
						artcontent.Artist.Ava	= json.sections[n].arts[m].artist.avatar;
						wall.Arts.push(artcontent);
						
						AppMgr.TotalCuratedImages++;
					}
				}
				if(wall.Arts.length > 0){
					Masterpieces.push(wall);
				}				
			}
		}
		//AppMgr.Debug($.toJSON(Masterpieces));
		AppMgr.MasterPage++;
		AppMgr.CreatePhotoTemp();
		if(Wall.WallType == 'masterpieces'){
			Wall.ActiveData	= Masterpieces;
			Wall.TotalWall	= Wall.ActiveData.length;
			if(SceneManager.ActiveScene == 'Title'){
				SceneManager.Collection[SceneManager.ActiveScene].UpdateContent();
			}
		}
	}
	else{
		AppMgr.Debug('Data is empty...');
		//error notification is here
	}
};

AppMgr.GetNextGallery = function(){		// auto retrieve gallery, run in background recursively
	AppMgr.Debug('Getting next gallery...');
	AppMgr.IsGettingGallery	= true;
	if(!AppMgr.TrendingAjax && !AppMgr.MasterAjax){
		if(AppMgr.TrendingPage <= AppMgr.TrendingMaxPage){
			AppMgr.GetTrending(AppMgr.TrendingPage, function(json, code, text){
				AppMgr.RetrieveTrending(json, code, text);				
				if(AppMgr.MasterPage <= AppMgr.MasterMaxPage){
					AppMgr.GetMasterpieces(AppMgr.MasterPage, function(json, code, text){	
						AppMgr.IsGettingGallery	= false;
						AppMgr.RetrieveMasterpieces(json, code, text);
						if(AppMgr.NetworkIsOn){
							AppMgr.GetNextGallery();
						}
						else{
							Platform.Debug('Pausing Data Retrieve...', 'red');
						}
					});
				}
				else{
					AppMgr.IsGettingGallery	= false;
					if(AppMgr.NetworkIsOn){
						AppMgr.GetNextGallery();
					}
					else{
						Platform.Debug('Pausing Data Retrieve...', 'red');
					}
				}
			});
		}
		else if(AppMgr.MasterPage <= AppMgr.MasterMaxPage){
			AppMgr.GetMasterpieces(AppMgr.MasterPage, function(json, code, text){	
				AppMgr.IsGettingGallery	= false;
				AppMgr.RetrieveMasterpieces(json, code, text);
				if(AppMgr.NetworkIsOn){
					AppMgr.GetNextGallery();
				}
				else{
					Platform.Debug('Pausing Data Retrieve...', 'red');
				}
			});
		}
		else{
			AppMgr.IsGettingGallery	= false;
			AppMgr.Debug('Nothing to retrieve');
		}
	}
	else{
		AppMgr.Debug('AJAX is busy: ' + AppMgr.TrendingAjax + ', ' + AppMgr.MasterAjax);
	}
};

AppMgr.CreatePhotoTemp = function(){
	var container	= document.getElementById('image-temp');
	var html		= '';
	for(var n=0; n<Trending.length; n++){
		for(var m=0; m<Trending[n].Arts.length; m++){
			html	+= '<img class="image-temp" src="'+ Trending[n].Arts[m].Image +'" />';
		}		
	}
	for(var n=0; n<Masterpieces.length; n++){
		for(var m=0; m<Masterpieces[n].Arts.length; m++){
			html	+= '<img class="image-temp" src="'+ Masterpieces[n].Arts[m].Image +'" />';
		}		
	}
	Platform.WidgetAPI.putInnerHTML(container, html);
};

AppMgr.Debug = function(m){
	Platform.Debug('[MASTERPIECES] ' + m, 'orange');
};