/* Project	: SEIN Smart Apps Tracking Module
 * File	    : seinAnalytics.js
 * Description
 * @fileoverview 	Module to track the usage of smart apps using Piwik
 * 					Open Source Web Tracking Software, Data Stored in SEIN
 *					Smart Apps Server at http://analytics.seinsmarttv.com
 *
 * @author I Made Krisna (made.krisna@samsung.com)
 *
 * Changelog    :
 * 01 2012-06-25 made.krisna 	Initial Code
 * 02 2012-06-27 made.krisna	Add In-Code Comments
 * 03 2012-06-27 made.krisna	[BETA RELEASE] Add Global Tracker
 * 04 2012-11-28 made.krisna	[RELEASE] Add support for 2013 Year Detection
 * 05 2013-07-22 made.krisna	Add Support for Google Analytics's ga.js
 * 06 2013-07-23 made.krisna	Fix ga.js SmartHub Version 'undefined' Bug
 * 								Auto Detect Apps Version from config.xml
 *
 * Copyright 2012 by Samsung Electronics, Inc.,
 * 
 * This software is the confidential and proprietary information
 * of Samsung Electronics, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Samsung.
 */


/* Class object for doing Smart Apps Analytics Tracking
 * @constructor
 */
var Analytics = new Object();

/* SEIN Analytics Module version, Formatted using:
 * YEAR.MONTH.DATE.BUILD_NUMBER
 * Initial Version Release: 2012.06.25.01 
 *
 * @type String
 * @const
 */
Analytics.VERSION = '2013.07.23.02';

/* Piwik Tracker End Point URL
 * @type String
 */
Analytics.ENDPOINT = 'http://analytics.seinsmarttv.com/piwik.php';

/* Required Constant for Enumaration in Module
 * @type Object
 * @const
 */
Analytics.CONSTANT 	= {
	UNKNOWN		: "UNKNOWN",					// ENUM for Unkown Value
	DEVICE_TYPE	: {								// ENUM for Device Type where Apps Running
		TV			: "DEVICE_TYPE_TV",
		MONITOR		: "DEVICE_TYPE_MONITOR",		
		BD			: "DEVICE_TYPE_BD"
	}
};

Analytics.gaOnly	= false;
Analytics.gaId 		= null;

/* Variable to indicate which apps tracked by the module, For Tracking Apps ID
 * Go to: http://analytics.seinsmarttv.com/index.php?module=SitesManager&action=index&idSite=1&period=day&date=today
 * @type Number
 */
Analytics.trackerId	= -1;

/* Piwik tracker object
 * Assigned by Piwik.getTracker() in initialize() function 
 * @type Object
 */
Analytics.piwik = null;

/* Piwik Global tracker object
 *
 */
Analytics.piwikGlobal = null;
 
/* Device Years based on Web Browser Engine used by apps (UserAgent)
 * @type Number
 */
Analytics.year 		= 0;

/* Device type (TV/Monitor/BD) which running apps
 * Refer to Analytics.CONSTANT.DEVICE_TYPE
 * @type String
 */
Analytics.type		= Analytics.CONSTANT.UNKNOWN;

/* Device Model/Product Code which running apps
 * Example: UA46ES7500_ED_DTV, SDK will produce LN40B650_KOR
 * @type String
 */
Analytics.model		= Analytics.CONSTANT.UNKNOWN;

/* Device INFOLINK Firmware Version
 * Example: T-INFOLINK2012-1000
 * @type String
 */
Analytics.firmware	= Analytics.CONSTANT.UNKNOWN;

/* Device Chipset Group Model
 * @type String
 */
Analytics.chipset	= Analytics.CONSTANT.UNKNOWN;

/* Device Smart Hub or Internet@TV Version
 * @type Number
 */
Analytics.smartHub	= 0;

Analytics.appVersion = 'unknown';

/* Required Plugin storage object for getting device information
 * The HTML object id cannot be changed, otherwise it will failed
 * @type Object
 */
Analytics.Plugin = {
	NNavi		: null,		// <object id='pluginObjectNNavi' 	classid='clsid:SAMSUNG-INFOLINK-NNAVI'></object>
	TV			: null,		// <object id='pluginObjectTV'		classid='clsid:SAMSUNG-INFOLINK-TV'></object>
	TVMW		: null		// <object id='pluginObjectTVMW'	classid='clsid:SAMSUNG-INFOLINK-TVMW'></object>
};

/* This function is used to set-up all of needed plugin and information
 * for future tracking of the application. This function must be called as soon as possible
 * after the seinAnalytics.js included in the project
 *
 * @param 	{Number} trackerId	Apps Tracking ID registered in analytics.seinsmarttv.com
 *								http://analytics.seinsmarttv.com/index.php?module=SitesManager&action=index&idSite=1&period=day&date=today
 * @return 	{Void}
 *
 * @example
 * Main.onLoad = function()
 * {
 * 		document.getElementById("anchor").focus();
 * 		widgetAPI.sendReadyEvent();
 *	
 *		Analytics.initialize(2);	// Track Apps with tracking ID = 2 (Test Apps Request)
 * } 
 *
 * @since Initial Version
 * @revision
 *	2012.06.27.01	Change Piwik.getTracker param1 to property Analytics.ENDPOINT
 *	2012.06.27.02	Add Global Tracker using Analytics.piwikGlobal
 *	2013.07.22.01	Add Support for Google Analytics's ga.js
 */
Analytics.initialize = function(trackerId, gaId){
	Analytics.Plugin.NNavi 		= document.getElementById('pluginObjectNNavi');			// Retrieve NNavi Plugin
	Analytics.Plugin.TV			= document.getElementById('pluginObjectTV');			// Retrieve TV Plugin
	Analytics.Plugin.TVMW		= document.getElementById('pluginObjectTVMW');			// Retrieve TVMW Plugin

	if (trackerId !== 'ga-only'){
		Analytics.trackerId 	= trackerId;													// Assign Apps Tracker ID
		Analytics.piwik			= Piwik.getTracker(Analytics.ENDPOINT, Analytics.trackerId);	// Get Piwik Tracker for Assigned Apps
		Analytics.piwikGlobal 	= Piwik.getTracker(Analytics.ENDPOINT, 1);						// Global tracking ID		
	}
	else{
		Analytics.gaOnly		= true;
	}		

	/* Detecting Running Device Information */
	Analytics.detectDeviceYear();
	Analytics.detectDeviceModel();
	Analytics.detectDeviceType();
	Analytics.detectFirmware();
	Analytics.detectChipset();
	Analytics.detectSmartHubVersion();
	Analytics.detectAppVersion();
	
	if (Analytics.gaOnly === false){
		/* Setting the custom variable for tracking based on device information */
		Analytics.piwik.setCustomVariable(1, 'Year', 		Analytics.year + '/' + Analytics.firmware);
		Analytics.piwik.setCustomVariable(2, 'Type', 		Analytics.type);
		Analytics.piwik.setCustomVariable(3, 'Chipset', 	Analytics.chipset);
		Analytics.piwik.setCustomVariable(4, 'Model', 		Analytics.model);
		Analytics.piwik.setCustomVariable(5, 'Smart Hub', 	Analytics.smartHub);
		
		Analytics.piwikGlobal.setCustomVariable(1, 'Year', 		Analytics.year + '/' + Analytics.firmware);
		Analytics.piwikGlobal.setCustomVariable(2, 'Type', 		Analytics.type);
		Analytics.piwikGlobal.setCustomVariable(3, 'Chipset', 	Analytics.chipset);
		Analytics.piwikGlobal.setCustomVariable(4, 'Model', 	Analytics.model);
		Analytics.piwikGlobal.setCustomVariable(5, 'Smart Hub', Analytics.smartHub);		
	}
	
	if (gaId && gaId.indexOf('-') > -1){
		Analytics.gaId = gaId;
	}		
};

/* Function to execute the tracking request
 * You can use this function in handleShow scene in Basic Project Apps or else
 *
 * @param 	{String} pageName		value of the page/scene/content which want to be tracked as pageviews
 * @return 	{Void}
 *
 * @example
 * SceneNewsSubtopic.prototype.handleShow = function () {
 * 		this.rewriteNews();	
 *		this.rewriteSubtopic();
 *		this.setHighlight();
 *	
 *		// Track NewsSubtopic Scene with additional active subtopic selected
 *		// In case of NewsSubtopic Scene is used by more than 1 content source
 *		Analytics.track('NewsSubtopic/' + activeSubtopic);
 * } 
 * 
 * @since Initial Version
 * @revision
 *	2012.06.27.02	Add Global Tracker using Analytics.piwikGlobal
 *	2012.07.09.01	Add Temporary Fix for Ungrouped Launched Apps: Cosmopolitan
 *	2013.07.22.01	Add Google Analytics's ga.js Support
 *	2013.07.23.01	Fix Bug for ga.js SmartHub Version 'undefined'
 */
Analytics.track = function(pageName){

	/* Temporary Fix for Cosmopolitan Apps */
	if (Analytics.trackerId === 3){
		pageName = 'Cosmopolitan/' + pageName;
	}

	if (Analytics.gaOnly === false){
		Analytics.piwik.setDocumentTitle(pageName);			// Emulate Page Title, Assign with Apps Scene or Content Viewed
		Analytics.piwik.trackPageView();					// Execute Tracking!
		
		Analytics.piwikGlobal.setDocumentTitle(pageName);
		Analytics.piwikGlobal.trackPageView();		
	}
	
	if (Analytics.gaId !== null){
		_gaq.push(['_setAccount', 	Analytics.gaId]);
		_gaq.push(['_setCustomVar',	1, 'Year', 		Analytics.type + '/' + Analytics.year, 1]);
		_gaq.push(['_setCustomVar',	2, 'Model', 	Analytics.model, 1]);
		_gaq.push(['_setCustomVar',	3, 'Chipset', 	Analytics.chipset, 1]);
		_gaq.push(['_setCustomVar',	4, 'Firmware', 	Analytics.firmware + '/' + Analytics.smartHub, 1]);
		_gaq.push(['_setCustomVar',	5, 'Version', 	Analytics.appVersion, 1]);
		
		_gaq.push(['_trackPageview', pageName]);
	}
};

/* ==================================== */
/* == INTERNAL USAGE FUNCTIONS BLOCK == */
/* ==================================== */

/* Function to detect device year group based on its web browser engine user agent
 * No need call this function manually for regular usage
 * this function always called in initialize()
 *
 * @return {Number} Device year, or 9999 if user agent identifier string not recognized
 *
 * @since Initial Version
 * @revision
 *	2012.11.28.01	Change year detection from user-agent to firmware value
 */
Analytics.detectDeviceYear = function(){

	var infolink 		= Analytics.detectFirmware();
	Analytics.year		= Number(infolink.substr(10, 4));
	
	/*
	var agent = window.navigator.userAgent;			// Retrieve Web Browser Engine User Agent value
	if (agent.indexOf('Maple2012') >= 0){			// Check for 2012 Browser Version
		Analytics.year = 2012;
	}
	else if (agent.indexOf('Maple 6') >= 0){		// Check for 2011 Browser Version
		Analytics.year = 2011;
	}
	else if (agent.indexOf('Maple 5') >= 0){		// Check for 2010 Browser Version
		Analytics.year = 2010;
	}
	else{											// Assuming unknown browser as more later version
		Analytics.year = 9999;
	}	
	*/
	
	return Analytics.year;
};

/* Function to detect device type (TV/Monitor/BD)
 * No need call this function manually for regular usage
 * this function always called in initialize()
 *
 * @return {String} Device type, refer possible value to Analytics.CONSTANT.DEVICE_TYPE and Analytics.CONSTANT.UNKNOWN
 *
 * @since Initial Version
 */
Analytics.detectDeviceType = function(){
	var value = Analytics.Plugin.TV.GetProductType();
	switch (value){
		case 0:
			Analytics.type = Analytics.CONSTANT.DEVICE_TYPE.TV;
			break;
		case 1:
			Analytics.type = Analytics.CONSTANT.DEVICE_TYPE.MONITOR;
			break;
		case 2:
			Analytics.type = Analytics.CONSTANT.DEVICE_TYPE.BD;
			break;
	}
	return Analytics.type;
};

/* Function to detect device infolink/firmware version
 * No need call this function manually for regular usage
 * this function always called in initialize()
 *
 * @return {String} Device INFOLINK/Firmware version
 *
 * @since Initial Version
 */
Analytics.detectFirmware = function(){
	Analytics.firmware = Analytics.Plugin.NNavi.GetFirmware();	
	return Analytics.firmware;
};

/* Function to detect chipset or model group
 * No need call this function manually for regular usage
 * this function always called in initialize()
 *
 * @return {String} Device chipset or model group
 *
 * @since Initial Version
 */
Analytics.detectChipset = function(){
	Analytics.chipset = Analytics.Plugin.NNavi.GetModelCode();
	return Analytics.chipset;
};

/* Function to detect device model code
 * No need call this function manually for regular usage
 * this function always called in initialize()
 *
 * @return {String} Device model code
 *
 * @since Initial Version
 */
Analytics.detectDeviceModel = function(){
	Analytics.model = Analytics.Plugin.TV.GetProductCode(1);
	return Analytics.model;
};

/* Function to detect SmartHub version of device currently use
 * No need call this function manually for regular usage
 * this function always called in initialize()
 *
 * @return {Number} SmartHub Version used
 *
 * @since Initial Version
 */
Analytics.detectSmartHubVersion = function(){
	var value 		= window.location.search;	
	var parameters	= value.split('&');
	for (var i = 0; i < parameters.length; i++){
		var keyvalue = parameters[i].split('=');
		if (keyvalue[0] === 'mgrver' || keyvalue[0] === '?mgrver'){
			Analytics.smartHub = keyvalue[1];
			break;
		}
	}
	return Analytics.smartHub;
};

Analytics.detectAppVersion = function(){
	File.Descriptor 		= File.System.openFile('config.xml', File.Mode.READ);	
	
	var content				= File.Descriptor.readAll();
	var xmlObject			= (new DOMParser()).parseFromString(content, 'text/xml');
	
	Analytics.appVersion	= xmlObject.getElementsByTagName('ver')[0].textContent;	
	File.System.closeFile(File.Descriptor);		
	return Analytics.appVersion;
};

