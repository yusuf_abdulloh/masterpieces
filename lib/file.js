/* File: file.js
 *
 * Description:
 * Module to access device filesystem and provide interface to do I/O Operation for JSON Format in common file
 *
 * Author:
 * I Made Krisna (made.krisna@samsung.com)
 *
 * Changelog:
 * - 2012-10-25 Initial Code (made.krisna)
 * - 2012-12-05 Add error handling for unparseable JSONLoad (made.krisna)
 */

/* Class: File
 * Filesystem Class object for doing File Access
 */
 
/* Constructor: File
 * Initialize the File Library Instance
 */
var File = new Object();

/* Variable: VERSION
 * File Library version, Formatted using: YEAR.MONTH.DATE.BUILD_NUMBER
 * 
 * Initial Version:
 * 2012.10.25.01 
 */
File.VERSION		= '2012.12.05.01';

/* Variable: DEBUG
 * Platform Debug Library Caller Flag, true to enable debug
 */
File.DEBUG			= true;

/* Variable: System
 * Samsung DTV Platform File System API Object
 * 
 * See Also:
 * - http://samsungdforum.com/Guide/ref00001/index.html
 */
File.System			= new FileSystem();

/* Variable: Directory
 * Common Directory Path
 *
 * See Also:
 * - <Platform.WidgetID>
 */
File.Directory		= Platform.WidgetID + '/';

/* Variable: Descriptor
 * File Pointer Object Storage
 */
File.Descriptor		= null;

/* Variable: String
 * String Buffer for File I/O Operation
 */
File.String			= '';

/* Variable: Object
 * JSON Object Buffer for File I/O Operation
 */
File.Object			= null;

/* Enumeration: Mode
 * File Open Mode Enumeration
 * 
 * Value:
 * File.Mode.READ - Open File for Reading
 * File.Mode.WRITE - Open File for Writing
 */
File.Mode = {
	READ	: 'r',
	WRITE	: 'w'
};

/* Function: JSONLoad
 * Function to Load JSON Saved Object from Common File
 *
 * Parameters:
 * {String} filename - Common Filename which will be loaded
 *
 * Returns:
 * {Object}	JSONObject - JSON Object from loaded file
 *
 * Example:
 * (start code)
 * var highscore = File.JSONLoad('highscore.data');
 * (end)
 *
 * Since:
 * Initial Version
 *
 * Revision:
 * - 2012.12.05.01 Add error handling for unparseable JSON format
 */
File.JSONLoad = function(filename){
	File.Debug('Loading File: ' + filename);
	File.Descriptor = File.System.openCommonFile(File.Directory + filename, File.Mode.READ);
	if (!File.Descriptor){
		File.Debug('--- ERROR: File Not Found!');		
		return null;
	}
	else{
		File.Debug('--- File Opened');
		File.String = '';
		File.Object	= null;
		
		File.String = File.Descriptor.readAll();
		File.Debug('File Loaded!');
		
		try {
			File.Object = eval('(' + File.String + ')');
		}
		catch (ex){
			File.Debug('--- File Read ERROR: Cannot Parse JSON Format');
			File.Object = null;
		}		
		File.System.closeCommonFile(File.Descriptor);
		
		return File.Object;
	}	
};

/* Function: JSONSave
 * Function to Save JSON Object to Common File
 *
 * Parameters:
 * {String} filename - Common Filename which will be saved
 * {Object} JSONObject - Javascript Object to be saved in common file
 *
 * Example:
 * (start code)
 * File.JSONSave('highscore.data', {score: 1000});
 * (end)
 *
 * Since:
 * Initial Version
 */
File.JSONSave = function(filename, JSONObject){
	File.Debug('Opening File for Saving: ' + filename);
	File.Descriptor = File.System.openCommonFile(File.Directory + filename, File.Mode.WRITE);
	if (!File.Descriptor){
		File.Debug('--- WARNING: File Not Exist');
		if (File.System.isValidCommonPath(Platform.WidgetID) !== 1){
			File.Debug('--- WARNING: Directory Not Exist, Creating...');
			File.System.createCommonDir(Platform.WidgetID);
		}
		
		File.Debug('--- Reopening File for Saving...');
		File.Descriptor = File.System.openCommonFile(File.Directory + filename, File.Mode.WRITE);
	}
	
	File.Debug('--- File Opened');
	
	File.String = $.toJSON(JSONObject);
	File.Descriptor.writeAll(File.String);	
	File.System.closeCommonFile(File.Descriptor);
	
	File.Debug('File Saved!');
	File.String = '';
};


/* Function: Debug
 * Function to call Platform.Debug() for debugging purpose, For internal purpose only, no need to call from apps
 *
 * Parameters:
 * {String} message - Message which will be printed in debugger
 *
 * Since:
 * Initial Version
 */
File.Debug = function(message){
	if (File.DEBUG === true){
		Platform.Debug('[FILE MANAGER] ' + message);
	}	
};