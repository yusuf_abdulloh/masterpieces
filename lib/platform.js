/** 
 * File: platform.js
 *
 * Description:
 * Common Platform feature wrapper including Common API, SEF Plugin and Frequently used Samsung Smart TV APIs
 *
 * Author:
 * I Made Krisna (made.krisna@samsung.com)
 *
 * Changelog:
 * - 2012-12-26 Initial Code (made.krisna)
 * - 2013-03-13 EnableVolumeOSD BannerState = 1 & Unregister VOL PANEL Key (made.krisna)
 * - 2013-04-30 Change Debugging System Color to Manual, Add second param (made.krisna)
 */

/**
 * Class: Platform
 * Platform Class object as common usage APIs wrapper
 */

/**
 * Constructor: Platform
 * Initialize the Platform Library Instance
 */ 
var Platform 	= new Object();

/** 
 * Variable: WidgetAPI
 * Instance of Smart TV API Object Common.API.Widget
 *
 * See Also:
 * - http://samsungdforum.com/Guide/ref00006/common_module_widget_object.html
 */
Platform.WidgetAPI	= new Common.API.Widget();

/** 
 * Variable: TvKey
 * Instance of Smart TV API Object Common.API.TVKeyValue
 *
 * See Also: 
 * - http://samsungdforum.com/Guide/ref00006/common_module_tvkeyvalue_object.html
 */
Platform.TvKey		= new Common.API.TVKeyValue();

/**
 * Variable: Plugin
 * Instance of Smart TV API Object Common.API.Plugin
 *
 * See Also: 
 * - http://samsungdforum.com/Guide/ref00006/common_module_plugin_object.html
 */
Platform.Plugin		= new Common.API.Plugin();

/**
 * Variable: WidgetID
 * The current Application ID, used for Convergence Application Connection
 */
Platform.WidgetID	= curWidget.id;

/**
 * Enumeration: MouseEvent
 * Remote Control Input KeyCode for Gesture/Mouse Event
 * 
 * Description:
 * Samsung Smart TV with Gesture Control enabled have additional event beside <Platform.TvKey>,
 * This event also handled as TvKey event but there is no enumeration available for this event on
 * the <Platform.TvKey> object
 *
 * Value:
 * Platform.MouseEvent.SCROLL_UP - Gesture Touch the Upper Side of Screen
 * Platform.MouseEvent.SCROLL_DOWN - Gesture Touch the Bottom Side of Screen
 * Platform.MouseEvent.SCROLL_LEFT - Gesture Touch the Left Side of Screen
 * Platform.MouseEvent.SCROLL_RIGHT - Gesture Touch the Right Side of Screen
 * 
 * See Also:
 * - <Platform.TvKey>
 */
Platform.MouseEvent	= {
	SCROLL_UP		: 40000002,
	SCROLL_DOWN		: 40000003,
	SCROLL_LEFT		: 40000004,
	SCROLL_RIGHT	: 40000005
};

/**
 * Constant: WIDTH
 * Define the application width resolution in pixels
 * 
 * Description:
 * Width of the application settings, the common used value is:
 * - 960px for 540p apps
 * - 1280px for 720p apps
 * 
 * Check also the resolution settings on the widget.info files and config.xml files
 * 
 */
Platform.WIDTH			= '1280px';

/**
 * Constant: HEIGHT
 * Define the application height resolution in pixels
 * 
 * Description:
 * Height of the application settings, the common used value is:
 * - 540px for 540p apps
 * - 720px for 720p apps
 * 
 * Check also the resolution settings on the widget.info files and config.xml files
 * 
 */
Platform.HEIGHT			= '720px';

/** 
 * Function: InitSEF
 * Initialize the embedded SEF Plugin object from index.html to <Platform.SEF> class
 * 
 * Description:
 * This function is used to prepare all of the common SEF Object Plugins, the plugins
 * may be used my the framework itself so it best to not removing any plugins definition
 * both on the index.html and in this file
 *
 * Example:
 * (start code)
 * Platform.InitSEF();
 * (end)
 * 
 * See Also:
 * - <Platform.SEF>
 * 
 */
Platform.InitSEF = function(){
	Platform.Debug('[PLATFORM] Initializing SEF Plugins');
	
	Platform.SEF.Player 	= document.getElementById('pluginObjectPlayer');
	Platform.SEF.Audio		= document.getElementById('pluginObjectAudio');
	Platform.SEF.TVMW		= document.getElementById('pluginObjectTVMW');
	Platform.SEF.Video		= document.getElementById('pluginObjectVideo');
	Platform.SEF.NNavi		= document.getElementById('pluginObjectNNavi');
	Platform.SEF.AppCommon	= document.getElementById('pluginObjectAppCommon');
	Platform.SEF.TV			= document.getElementById('pluginObjectTV');
	Platform.SEF.Network	= document.getElementById('pluginObjectNetwork');
	
	Platform.SetScreenSaver(true);
	for (var key in Platform.SEF){
		if (!Platform.SEF[key]){
			Platform.Debug('[PLATFORM] WARNING: SEF Plugin ' + key + ' Uninitialized', 'yellow');
		}
		else{
			Platform.Debug('[PLATFORM] SEF Plugin ' + key + ': OK', 'green');
		}		
	}
};

/**
 * Function: EnableVolumeOSD
 * Function to Enable TV Volume On-Screen Display support to control volume
 *
 * Description:
 * This function will automatically enabling the Samsung Built-In Volume OSD Control,
 * After using this function, the application lose control for Volume Up/Down and Mute button.
 * This function requires <Platform.SEF.NNavi>, <Platform.SEF.TVMW>, and <Platform.Plugin>.
 * 
 * Using the Samsung Volume OSD will bring more defect-less application related to volume policy,
 * If the developers need to use their own volume UI, please look at <Platform.SEF.Audio>
 *
 * Example:
 * (start code)
 * Platform.EnableVolumeOSD();
 * (end)
 *
 * See Also:
 * - <Platform.SEF.Audio>
 */
Platform.EnableVolumeOSD = function(){
	Platform.SEF.NNavi.SetBannerState(1);
	Platform.Plugin.unregistKey(Platform.TvKey.KEY_VOL_UP);
	Platform.Plugin.unregistKey(Platform.TvKey.KEY_VOL_DOWN);
	Platform.Plugin.unregistKey(Platform.TvKey.KEY_PANEL_VOL_UP);
	Platform.Plugin.unregistKey(Platform.TvKey.KEY_PANEL_VOL_DOWN);	
	Platform.Plugin.unregistKey(Platform.TvKey.KEY_MUTE);
	Platform.Debug('[PLATFORM] Enabling Volume OSD...');
};

/**
 * Function: SetScreenSaver
 * Function to set TV Screen Saver is on or off
 * 
 * Description:
 * Setting Screen Saver is useful to comply with Samsung Policy about Screen Saver
 * on playing Game or VOD, Set screen saver to off when VOD played and set it back
 * to on again after the VOD finished
 *
 * Parameters:
 * {Boolean} state - true = set on, false = set off
 *
 * Example:
 * (start code)
 * function playVideo(){
 *		Platform.SEF.Player.StartPlayback();
 *		Platform.SetScreenSaver(false);
 * }
 * (end)
 */
Platform.SetScreenSaver = function(state){
	if (state === true){
		Platform.Plugin.setOnScreenSaver();
		Platform.State.ScreenSaver = true;
	}
	else{
		Platform.Plugin.setOffScreenSaver();
		Platform.State.ScreenSaver = false;
	}
	Platform.Debug('[PLATFORM] Set ScreenSaver: ' + Platform.State.ScreenSaver);
};

/**
 * Function: CheckNetwork
 * Function to check the TV Network Connection available or not
 *
 * Description:
 * Check Network only works to check the physical network status, if there
 * is a network down from ISP, this function won't be able to detect the
 * problem and still returning status true
 *
 * Returns:
 * {Boolean} true - Network Available
 * {Boolean} false - Network Not Available
 *
 * Example:
 * (start code) 
 * if (Platform.CheckNetwork() === false){
 *		alert('Network Connection Not Available');
 * }
 * (end) 
 */
Platform.CheckNetwork = function(){
	var activeConn = Platform.SEF.Network.GetActiveType();
	if (activeConn === -1){
		return false;
	}
	else{
		var phyConn = Platform.SEF.Network.CheckPhysicalConnection(activeConn);
		if (phyConn === 1){
			return true;
		}
		else{
			return false;
		}
	}
};

/**
 * Function: GetIPAddress
 * Function to get the TV Local IP Address of active network connection
 *
 * Description:
 * This function will return a string of Local IP Address of the TV. It is not possible
 * to get the public IP Address with Samsung Device API, to get the public IP Address please
 * look for any web services that provide public api to check the public IP Address 
 *
 * Returns:
 * {String} IP Address - TV Local IP Address
 * {null} - No Network Available
 *
 * Example:
 * (start code) 
 * alert('TV IP Address: ' + Platform.GetIPAddress()); 
 * (end) 
 *
 * See Also:
 * - <Platform.GetMACAddress>
 */
Platform.GetIPAddress = function(){
	if (Platform.CheckNetwork() === true){
		return Platform.SEF.Network.GetIP(Platform.SEF.Network.GetActiveType());
	}
	else{
		return null;
	}
};

/**
 * Function: GetMACAddress
 * Function to get the MAC Address of active network connection
 * 
 * Description:
 * This function will return a string of active connection MAC Address, The device
 * may have 2 network connection (Cable and Wireless) so if developers need to use
 * unique ID for each device, please look for <Platform.GetUID>
 * 
 * Returns:
 * {String} MAC Address - TV Network MAC Address
 * {null} - No Network Available
 *
 * Example:
 * (start code) 
 * alert('TV MAC Address: ' + Platform.GetMACAddress());  
 * (end) 
 * 
 * See Also:
 * - <Platform.GetUID>
 * 
 */
Platform.GetMACAddress = function(){
	if (Platform.CheckNetwork() === true){
		return Platform.SEF.Network.GetMAC(Platform.SEF.Network.GetActiveType());
	}
	else{
		return null;
	}
};

/**
 * Function: GetUID
 * Function to get device unique ID based on wired network mac address
 *
 * Returns:
 * {String} DUID - Device Unique ID
 *
 * Example:
 * (start code)
 * alert('TV Unique ID: ' + Platform.GetUID());  
 * (end)
 */
Platform.GetUID = function(){
	return Platform.SEF.NNavi.GetDUID(Platform.SEF.Network.GetMAC());
};

/* Function: Block
 * Function wrapper for <Platform.WidgetAPI>.blockNavigation(event)
 *
 * Example:
 * (start code)
 * switch (keyCode){
 *	case Platform.TvKey.KEY_RETURN:
 *		Platform.Block();
 *		break;
 * }
 * (end)
 *
 * See Also:
 * - http://samsungdforum.com/Guide/ref00006/common_module_widget_object.html#ref00006-common-module-widget-object-blocknavigation
 */
Platform.Block = function(){
	Platform.WidgetAPI.blockNavigation(event);
};

/**
 * Function: GetDeviceYear
 * Function to get the device or platform released year based on the firmware (INFOLINK)
 *
 * Returns:
 * {Number} Year - 4 digit Device Year
 *
 * Example:
 * (start code)
 * if (Platform.GetDeviceYear() !== 2012){
 * 		alert('The application only support device for 2012 platform');
 * }
 * (end)
 *
 * See Also:
 * - <Platform.GetFirmware>
 */
Platform.GetDeviceYear = function(){
	var firmware 	= Platform.GetFirmware();
	var fwcode		= firmware.replace('T-INFOLINK', '');
	var fwyear		= fwcode.split('-');
	return Number(fwyear[0]);
};

/* Function: GetFirmware
 * Function to get INFOLINK Firmware version
 *
 * Returns:
 * {String} infolink - Infolink Firmware Version
 *
 * Example:
 * (start code)
 * alert('Firmware Code: ' + Platform.GetFirmware());
 * (end)
 *
 * See Also:
 * - http://samsungdforum.com/Guide/ref00014/GetFirmware.html
 */
Platform.GetFirmware = function(){
	return Platform.SEF.NNavi.GetFirmware();
};

/* Function: GetDeviceModel
 * Function to get device model where application running
 *
 * Returns:
 * {String} devicemodel - Device Model, ex: UA55ES8000_USA
 *
 * Example:
 * (start code)
 * alert('Device Model Code: ' + Platform.GetDeviceModel());
 * (end)
 *
 * See Also:
 * - http://samsungdforum.com/Guide/ref00014/GetProductCode.html
 */
Platform.GetDeviceModel = function(){
	return Platform.SEF.TV.GetProductCode(1);
};

/* Function: GetSmartHubVersion
 * Function to get the Smart Hub Version where application running
 *
 * Returns:
 * {String} smarthub - Smart Hub Version
 * 
 * Example:
 * (start code)
 * alert('Smart Hub Version: ' + Platform.GetSmartHubVersion());
 * (end)
 */
Platform.GetSmartHubVersion = function(){
	var version		= null;
	var value 		= window.location.search;
	var parameters	= value.split('&');
	for (var i = 0; i < parameters.length; i++){
		var keyvalue = parameters[i].split('=');
		if (keyvalue[0] === 'mgrver' || keyvalue[0] === '?mgrver'){
			version = keyvalue[1];
			break;
		}
	}
	return version;
};

/**
 * Function: GetLocalPath
 * Function to get absolute path for passed to <Platform.SEF.Player>.InitPlayer function
 *
 * Description:
 * Local Path function is used to developer who want to get absolute path of the files inside
 * application. For example if the developer want to play video or audio from project folder
 * using the <Platform.SEF.Player>
 *
 * Parameters:
 * {String} linkString - relative path from application folder
 *
 * Returns:
 * {String} path - absolute path for media player initialize
 *
 * Example:
 * (start code)
 * var audioPath = Platform.GetLocalPath('audio/bgmusic.mp3');
 * Platform.SEF.Player.InitPlayer(audioPath);
 * (end)
 *
 */
Platform.GetLocalPath = function(linkString){
	var Abs_path = '';
	var rootPath = window.location.href.substring(0, location.href.lastIndexOf("/")+1);
	if (unescape(window.location.toString()).indexOf('localhost') == -1) {	
		if (unescape(window.location.toString()).indexOf('file://C') != -1){
			Abs_path = unescape(rootPath).split('file://')[1].replace('C/','C:/')+linkString;
		}
		else { 
			Abs_path = unescape(rootPath).split('file://')[1]+linkString;
		}
	}
	else {
		if (unescape(window.location.toString()).indexOf('C:') != -1){ 
			Abs_path = '/' + unescape(rootPath).split('file://localhost/C:\\')[1].replace(/\\/g,'/')+linkString;
		}
		else { 
			Abs_path = '/' + unescape(rootPath).split('file://localhost/')[1]+linkString;
		}
	}
	return Abs_path;
};

/*===============================================================*/
/*================== Platform Debugging Engine ==================*/
/*===============================================================*/

Platform.DEBUG		= true;
Platform.DEBUG_UI	= false;
Platform.DEBUG_ID	= curWidget.id + '_debug';
Platform.DEBUG_LOG	= new Array();
Platform.DEBUG_STR	= '';
Platform.DEBUG_LEN	= 50;

Platform.InitDebug = function(mode){
	switch (mode){
		case 'debugging':
			Platform.DEBUG 		= true;
			Platform.DEBUG_UI	= true;
			break;
		case 'development':
			Platform.DEBUG		= true;
			Platform.DEBUG_UI	= false;
			break;
		case 'production':
			Platform.DEBUG		= false;
			Platform.DEBUG_UI	= false;
			break;
	}

	if (Platform.DEBUG === true && Platform.DEBUG_UI === true){
		$('body').append('<div style="position:absolute;top:0px;left:0px;width:' + Platform.WIDTH + ';height:' + Platform.HEIGHT + ';z-index:99;background-color:rgba(0,0,0,0.5);color:white;"><div id="' + Platform.DEBUG_ID + '" style="position:absolute;bottom:10px;left:0px;"></div></div>');
		Platform.Debug('[PLATFORM] Embedding Debug UI Element: ' + Platform.DEBUG_ID);
	}
	Platform.Debug('[PLATFORM] Debug Mode Enabled: '   + Platform.DEBUG);	
	Platform.Debug('[PLATFORM] --- Debug UI Enabled: ' + Platform.DEBUG_UI);
};

Platform.SetDebug = function(flag){
	Platform.DEBUG = flag;
};

Platform.SetDebugUI = function(flag){
	Platform.DEBUG_UI = flag;
};

Platform.Debug = function(message, color){
	if (Platform.DEBUG === true){
		alert('### DEBUG ### ' + message);
		if (Platform.DEBUG_UI === true){			
			if (!color){
				color = 'white';
			}
			message = '<span style="color:' + color + '">' + message + '</span>';
			
			Platform.DEBUG_LOG.push(message);
			
			while(Platform.DEBUG_LOG.length > Platform.DEBUG_LEN){
				Platform.DEBUG_LOG.splice(0,1);
			}
			
			Platform.DEBUG_STR = '';
			for (var i = 0; i < Platform.DEBUG_LOG.length; i++){
				var content = Platform.DEBUG_LOG[i];
				
				Platform.DEBUG_STR += content;
				if (i !== Platform.DEBUG_LOG.length - 1){
					Platform.DEBUG_STR += '<hr />';
				}				
			}
			Platform.WidgetAPI.putInnerHTML(document.getElementById(Platform.DEBUG_ID), Platform.DEBUG_STR);
		}
	}	
};

/* Class: Platform.State
 * Object Collection for describing the state of application
 */
Platform.State = {
	/* Variable: ScreenSaver
	 * Boolean variable describing is the screen saver is enabled or not
	 */
	ScreenSaver	: true
};

/* Class: Platform.SEF
 * Object Collection for storing SEF Plugin instance
 *
 * See Also:
 * - http://samsungdforum.com/Guide/ref00014/index.html
 */
Platform.SEF = {
	/* Variable: Player
	 * SEF Plugin for Playing Media Files (Audio/Video/Image)
	 *
	 * See Also:
	 * - http://samsungdforum.com/Guide/ref00014/sef_plugin_player.html
	 */
	Player		: null,
	
	/* Variable: Audio
	 * SEF Plugin for Controlling TV Audio related feature
	 *
	 * See Also:
	 * - http://samsungdforum.com/Guide/ref00014/sef_plugin_audio.html
	 */	
	Audio		: null,
	
	/* Variable: TVMW
	 * SEF Plugin for Handles the commands for basic application such as language, country, key registration, and so on, on the DTV platform.
	 *
	 * See Also:
	 * - http://samsungdforum.com/Guide/ref00014/sef_plugin_tvmw.html
	 */		
	TVMW		: null,
	
	/* Variable: Video
	 * SEF Plugin for Controls the video-related commands on the DTV platform (for example, Screen state).
	 *
	 * See Also:
	 * - http://samsungdforum.com/Guide/ref00014/sef_plugin_video.html
	 */			
	Video		: null,
	
	/* Variable: NNavi
	 * SEF Plugin for controls Samsung Smart TV-specific functions on the DTV platform (for example, DUID, ServerType, and so on).
	 *
	 * See Also:
	 * - http://samsungdforum.com/Guide/ref00014/sef_plugin_nnavi.html
	 */			
	NNavi		: null,
	
	/* Variable: AppCommon
	 * SEF Plugin for the basic TV functions (for example, Key Registration, and so on).
	 *
	 * See Also:
	 * - http://samsungdforum.com/Guide/ref00014/sef_plugin_appcommon.html
	 */			
	AppCommon	: null,
	
	/* Variable: TV
	 * SEF Plugin for handles the basic TV commands (for example, EPG, Callback event, and so on).
	 *
	 * See Also:
	 * - http://samsungdforum.com/Guide/ref00014/sef_plugin_tv.html
	 */		
	TV			: null,
	
	/* Variable: Network
	 * SEF Plugin for controls and gets network-related information about the DTV platform.
	 *
	 * See Also:
	 * - http://samsungdforum.com/Guide/ref00014/sef_plugin_network.html
	 */		
	Network		: null
};