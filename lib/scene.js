/* File: scene.js
 *
 * Description:
 * Scene Manager Object which control the application functionality, Scenes & Plugins Management, UI Display, and Input Control
 *
 * Author:
 * I Made Krisna (made.krisna@samsung.com)
 *
 * Changelog:
 * - 2012-12-26 Initial Code (made.krisna)
 * - 2013-02-15 Fix Scene Redirection problem :: Scene1.focus() -> ChangeTo('Scene2') (made.krisna)
 */

/* Class: SceneManager
 * SceneManager class to enable the Basic-Project like feature
 */

/* Constructor: Platform
 * Initialize the Platform Library Instance
 */  
var SceneManager = new Object();

/* Variable: DEBUG
 * Set the function <SceneManager.Debug> is working or no
 */
SceneManager.DEBUG			= true;

/* Variable: PLUGIN_PATH
 * The relative path of available plugins folder
 */
SceneManager.PLUGIN_PATH	= 'plugins/';

/* Variable: SCENE_PATH
 * The relative path of available scenes folder
 */
SceneManager.SCENE_PATH		= 'scenes/';

SceneManager.SCENE_HTML		= SceneManager.SCENE_PATH + 'html/';
SceneManager.SCENE_CSS		= SceneManager.SCENE_PATH + 'css/';
SceneManager.SCENE_JS		= SceneManager.SCENE_PATH + 'js/';

SceneManager.WIDTH			= Platform.WIDTH;
SceneManager.HEIGHT			= Platform.HEIGHT;

SceneManager.Initialized 	= new Array();

/* Variable: Collection
 * Array of Initialized Scene Objects, stored for scene management purpose (show/focus/hide)
 */
SceneManager.Collection		= new Object();

/* Variable: ActiveScene
 * String of current focused scene name
 */
SceneManager.ActiveScene 	= null;

SceneManager.PreInit = function(scene){
	SceneManager.Debug('Pre-Init Scene: ' + scene);

	var init = false;
	for (var i = 0; i < SceneManager.Initialized.length; i++){
		if (scene === SceneManager.Initialized[i]){
			init = true;
			break;
		}
	}
	
	if (init === false){
		SceneManager.Debug('--- Scene Uninitialized, Initializing Scene');
	
		SceneManager.Debug('------ Loading Scene JavaScript');
		$('head').append('<script language="javascript" type="text/javascript" src="' + SceneManager.SCENE_JS + scene + '.js"></script>');
		
		SceneManager.Debug('------ Loading Scene CSS');
		$('head').append('<link rel="stylesheet" href="' + SceneManager.SCENE_CSS + scene + '.css" type="text/css">');

		SceneManager.Debug('------ Loading Scene HTML');
		File.Descriptor = File.System.openFile(SceneManager.SCENE_HTML + scene + '.html', File.Mode.READ);
		$('body').append('<div id="platformscene_' + scene + '" style="display:none;position:absolute;top:0px;left:0px;width:' + SceneManager.WIDTH + ';height:' + SceneManager.HEIGHT + ';overflow:hidden">' + File.Descriptor.readAll() + '</div>');
		File.System.closeFile(File.Descriptor);
		
		SceneManager.Initialized.push(scene);
		SceneManager.Collection[scene] = eval('new '+ scene + '()');
		SceneManager.Collection[scene].initialize();
		SceneManager.Debug('--- Scene Initialized');
	}
	else{
		SceneManager.Debug('--- Scene Already Initialized, Skipping...');
	}
};

/* Function: Show
 * Function to display the scene UI to the screen
 *
 * Parameters:
 * {String} scene - Scene name to be shown
 *
 * See Also:
 * - <SomeScene.handleShow>
 */
SceneManager.Show = function(scene){
	SceneManager.Debug('Show Scene: ' + scene);

	var init = false;
	for (var i = 0; i < SceneManager.Initialized.length; i++){
		if (scene === SceneManager.Initialized[i]){
			init = true;
			break;
		}
	}
	
	if (init === false){
		SceneManager.Debug('--- Scene Uninitialized, Initializing Scene');
	
		SceneManager.Debug('------ Loading Scene JavaScript');
		$('head').append('<script language="javascript" type="text/javascript" src="' + SceneManager.SCENE_JS + scene + '.js"></script>');
		
		SceneManager.Debug('------ Loading Scene CSS');
		$('head').append('<link rel="stylesheet" href="' + SceneManager.SCENE_CSS + scene + '.css" type="text/css">');

		SceneManager.Debug('------ Loading Scene HTML');
		File.Descriptor = File.System.openFile(SceneManager.SCENE_HTML + scene + '.html', File.Mode.READ);
		$('body').append('<div id="platformscene_' + scene + '" style="display:block;position:absolute;top:0px;left:0px;width:' + SceneManager.WIDTH + ';height:' + SceneManager.HEIGHT + ';overflow:hidden">' + File.Descriptor.readAll() + '</div>');
		File.System.closeFile(File.Descriptor);
		
		SceneManager.Initialized.push(scene);
		SceneManager.Collection[scene] = eval('new '+ scene + '()');
		SceneManager.Collection[scene].initialize();
		SceneManager.Debug('--- Scene Loaded');
	}
	else{
		$('#platformscene_' + scene).css('display', 'block');
	}
	SceneManager.Collection[scene].handleShow();
};

/* Function: Focus
 * Function to give the called scene with input handler and make as <ActiveScene>
 *
 * Parameters:
 * {String} scene - Scene name to be focused
 *
 * Note:
 * - Called scene must have been initialized
 *
 * See Also:
 * - <SceneManager.Show>
 * - <SomeScene.handleFocus>
 */
SceneManager.Focus = function(scene){	
	SceneManager.Debug('Focus Scene: ' + scene);
	SceneManager.ActiveScene = scene;
	SceneManager.Collection[scene].handleFocus();	
};

/* Function: Hide
 * Function to remove the scene UI from the screen
 *
 * Parameters:
 * {String} scene - Scene name to be hidden
 *
 * Note:
 * - Called scene must have been initialized
 *
 * See Also:
 * - <SceneManager.Show>
 * - <SomeScene.handleHide>
 */
SceneManager.Hide = function(scene){
	SceneManager.Debug('Hide Scene: ' + scene);
	$('#platformscene_' + scene).css('display', 'none');
	SceneManager.Collection[scene].handleHide();
};

/* Function: ChangeTo
 * Function to wrap sequence: Show, Focus, and Hide. Show and Focus called scene and Hide current active scene
 *
 * Parameters:
 * {String} scene - Scene name to be hidden
 *
 * See Also:
 * - <SceneManager.Show>
 * - <SceneManager.Focus>
 * - <SceneManager.Hide>
 */
SceneManager.ChangeTo = function(scene){
	var curScene = SceneManager.ActiveScene;
	SceneManager.Show(scene);
	SceneManager.Focus(scene);
	SceneManager.Hide(curScene);
};

/* Function: LoadPlugin
 * Function to load and enable plugins located on <PLUGIN_PATH>, The plugins name is same as folder name
 *
 * Parameters:
 * {String} plugin - Plugin folder name
 */
SceneManager.LoadPlugin = function(plugin){
	SceneManager.Debug('Loading Plugin: ' + plugin);
	File.Descriptor = File.System.openFile(SceneManager.PLUGIN_PATH + plugin + '/config.json', File.Mode.READ);	
	var config 		= eval('(' + File.Descriptor.readAll() + ')');
	File.System.closeFile(File.Descriptor);
	SceneManager.Debug('--- Plugin config.json loaded!');

	SceneManager.Debug('--- Loading plugin JavaScript...');
	for (var i = 0; i < config.js.length; i++){
		$('head').append('<script language="javascript" type="text/javascript" src="' + SceneManager.PLUGIN_PATH + plugin + '/' + config.js[i] + '"></script>');
		SceneManager.Debug('------ Loaded: ' + config.js[i]);
	}
	
	SceneManager.Debug('--- Loading plugin CSS...');
	for (var i = 0; i < config.css.length; i++){
		$('head').append('<link rel="stylesheet" href="' + SceneManager.PLUGIN_PATH + plugin + '/' + config.css[i] + '" type="text/css">');
		SceneManager.Debug('------ Loaded: ' + config.css[i]);
	}
	
	SceneManager.Debug('--- Loading plugin HTML...');
	for (var i = 0; i < config.html.length; i++){
	
		File.Descriptor = File.System.openFile(SceneManager.PLUGIN_PATH + plugin + '/' + config.html[i], File.Mode.READ);
		$('body').append(File.Descriptor.readAll());
		File.System.closeFile(File.Descriptor);		
		SceneManager.Debug('------ Loaded: ' + config.html[i]);
	}
};

/* Function: Debug
 * Display On-Screen Debug using <Platform.Debug> function
 *
 * See Also:
 * - <Platform.Debug>
 */
SceneManager.Debug = function(message, color){
	if (SceneManager.DEBUG){
		Platform.Debug('[SCENE MANAGER] ' + message, color);
	}
};